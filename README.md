# Diversity in Science is a project to systematically collect the publications in a field and analyze the association between diversity in participants and innovation in ideas

We are targeting two intellectual communities: machine learning and computational linguistics.
Generally, all scripts describe their options with `--help`, and should run with defaults.

## Data collection

With the exception of the ACL Anthology Network (containing aclweb publications through 2014), publications are available online in well-structured formats, but not in a processed or centralized form. So we need to acquire paper metadata and fulltext.

Status:

- ICML: Complete
- NIPS: Complete
- ACLWEB: Have aan corpus (through 2014), have not scraped the rest.
- Women in ML: Downloaded abstracts; not processed.
- Black in AI: Can't find abstracts.
- LatinX in AI: Can't find abstracts.

### Scraping metadata

First, metadata about publications is scraped and stored in bibtex format. The scripts for doing this are a bit messy, as the task is not too generalizable. The scripts for scraping metadata include:

- `proceedings.py` with base classes for scraping
- `acm_proceedings.py` subclassing base classes for ACM proceedings pages
- `citation_parser.py` with parsing helpers
- `helpers.py` general utility functions
- `scrape_icml_bibs.py` runs the full scrape for ICML
- `scrape_nips_bibs.py` runs the full scrape for NIPS

Additionally, the `bibtools` package contains utilities for interacting with bibtex entries.

### Extracting text

Second, we need to extract the full text from publications. This involves downloading PDFs and extracting their text. I tested several text extraction utilities (TET and pdfminer), and found pdfminer, which is free, to yield faithful text extraction.

- `extract.py` reads bibliographies, downloads PDFs, and extracts full text from them. Configuration options are explained via `--help`.

### Extracting authors

We need to extract author names so we can use Bas's algorithm to infer covariate race and gender of publication authors.
Use Bas's algorithm to infer covariate race and gender of publication authors. I currently have two strategies for this:

1. Extract bibliographies to get author names with `extract_authors_from_bibs.py`.
2. Extract author names and affiliations from paper fulltext with `extract_authors_from_text.py`. 
   (This additional data might yield richer inferences, and could be useful in later analysis)

## Analysis

### Phrase extraction

Topic modeling will be improved if we can use phrases instead of just words. 

- `extract_phrases.py` uses [AutoPhrase](https://github.com/shangjingbo1226/AutoPhrase) to do phrase extraction. It writes the fulltext corpus in the format required for AutoPhrase, and (soon will) run AutoPhrase. 

Following David Jurgens's August 20, 2018 email to Dan McFarland, we could also use El-Kishky, et al's (2015) [ToPMine](http://web.engr.illinois.edu/~elkishk2/). 
However, AutoPhrase is the successor to SegPhrase+, which Liu, et al (2015) found to be substantially better than ToPMine.

### Exploratory analysis

Run LDA on the corpus to see what kinds of topics come out. 

### Structural topic modeling

Use Roberts et al's `STM` R package on the ICML+NIPS corpus, with year, venue, and inferred author properties (gender, race) as 
covariates. Bryan's `innovation` package contains a script for incremental-saving STM training on Sherlock.

### Citation extraction

In order to construct collaboration or citation networks, we need paper citations. 
Dan Jurafsky, who would know, said citation extraction is a real pain and requires a lot of manual correction. So I'm going to hold off on this for now unless our research questions require exploring it. 

### Hyperparameter search

#### Number of topics (K)

TODO: Compute internal and external validity.

Here's one [idea for resolving citations](https://www.crossref.org/labs/resolving-citations-we-dont-need-no-stinkin-parser/); python also has a library called refextract that might be worth exploring.

## Installation

- Clone this repo
- Make a virtualenv: `python3 -m venv env`
- Activate it: `source env/bin/activate`
- Install and build external dependencies: [Libpostal](https://github.com/openvenues/libpostal), [AutoPhrase](https://github.com/shangjingbo1226/AutoPhrase)
- Install dependencies: `pip install -r requirements.txt`

## Notes
- NIPS seems to actually be missing a bunch of textfiles...

What am I doing now? 
1. Train models across NumTopics, collecting diagnostics and holding out 10%.
2. Plot coherence, exclusivity, and FREX (harmonic mean), as well as perplexity overholdouts. 
3. Use the best model to re-run the analysis.
    A. Try using all authors of a paper, with some distribution over the paper credit. 
4. See if I can find some kind of external validation.

from bibtools.reader import bib_dir_reader
from extract_authors_from_text import name_cleaner_factory
from itertools import chain, tee
from pathlib import Path
import pandas as pd


clean = name_cleaner_factory()

def first_author_reader(entries):
    for e in entries:
        try:
            auth = e.persons.get('author')[0]
            yield (auth.first_names, auth.middle_names, auth.last_names)
        except TypeError:
            yield ([], [], [])

def year_reader(entries):
    for e in entries:
        yield e.fields['year']

def fulltext_reader(entries, venue):
    for e in entries:
        txtfile = Path('../results/text/') / venue / (e.key + '.txt')
        if txtfile.exists():
            with open(txtfile) as txt:
                yield txt.read()
        else:
            yield ""

icmlReader = bib_dir_reader('../results/bibliographies/icml')
r0, r1, r2, r3, r4,r5 = tee(icmlReader, 6)
icml = pd.DataFrame({
    'first': [clean(' '.join(e[0])) for e in first_author_reader(r0)],
    'middle': [clean(' '.join(e[1])) for e in first_author_reader(r1)],
    'last': [clean(' '.join(e[2])) for e in first_author_reader(r2)],
    'year': year_reader(r3),
    #'text': fulltext_reader(r4, 'icml'),
    'venue': 'icml',
}, index = [pub.key for pub in r5])

nipsReader = bib_dir_reader('../results/bibliographies/nips')
r0, r1, r2, r3, r4,r5 = tee(nipsReader, 6)
nips = pd.DataFrame({
    'first': [clean(' '.join(e[0])) for e in first_author_reader(r0)],
    'middle': [clean(' '.join(e[1])) for e in first_author_reader(r1)],
    'last': [clean(' '.join(e[2])) for e in first_author_reader(r4)],
    'year': [e.fields['year'] for e in r3],
    #'text': fulltext_reader(r4, 'nips'),
    'venue': 'nips',
}, index = [pub.key for pub in r5])

df = pd.concat([icml, nips])

inferenceDF = pd.read_csv('../results/name_inferences.csv', 
        usecols=['first', 'middle', 'last', 'race_usa', 'gender'],
        index_col=['first', 'middle', 'last'])

df = df.merge(inferenceDF, how='left', left_index=True, right_index=True)
df.to_csv('stm.csv')

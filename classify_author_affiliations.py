# We extracted authors' affiliations from scraped text. Now we want to classify them as 
# academic, corporate, military, other.

# After hand-labeling 500 authors' affiliations, we can use a naive bayes classifier
# to do the rest.

import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from pathlib import Path

LABELED_AUTHORS_FILE = Path(__file__).resolve().parent.parent / 'results' / 'authors' / 'authors_from_text.handlabeled.csv'
AUTHORS_FILE = Path(__file__).resolve().parent.parent / 'results' / 'authors' / 'authors_from_text.classified.csv'
CLASSES = ["academic", "corporate", "military", "other"]
authors = pd.read_csv(LABELED_AUTHORS_FILE)
authors = authors[authors.place.notnull()]

def evaluate(model):
    "Train on whatever labeled data we have; see how well it does"
    authors_labeled = authors[authors.classification.notnull()]
    authors_train, authors_test = train_test_split(authors_labeled, test_size = 0.2)
    model.fit(authors_train.place, authors_train.classification)
    return model.score(authors_test.place, authors_test.classification)

def extrapolate(model):
    "Use the classifier to fill in missing values"
    authors_labeled = authors[authors.classification.notnull()]
    authors_unlabeled = authors[~ authors.classification.notnull()]
    model.fit(authors_labeled.place, authors_labeled.classification)
    authors['classification'] = model.predict(authors.place)

def add_continent():
    "Having nothing to do with this module, we still stick it here.."
    from country_converter import CountryConverter
    cc = CountryConverter()
    authors['continent'] = cc.convert(authors.country.tolist(), to='continent', not_found="Unknown")
    
    
if __name__ == '__main__':
    model = Pipeline([
        ('vectorizer', CountVectorizer(analyzer='char', ngram_range=(2, 8))),
        ('classifier', MultinomialNB())
    ])

    print("{} of {} records labeled.".format(len(authors[authors.classification.notnull()]), len(authors)))
    mean_score = sum([evaluate(model) for i in range(10)]) / 10
    print(mean_score)

    extrapolate(model)
    add_continent()
    authors.to_csv(AUTHORS_FILE)

# Many ICML conferences unfortunately don't have standardized ways of getting their bib files.
# These functions get the job done. 

from pybtex.database import BibliographyData, Entry, parse_string, Person, parse_file
from bs4 import BeautifulSoup
import requests
import re
from helpers import *
from tqdm import tqdm
from fuzzywuzzy import process
from acm_proceedings import ACMProceedings
import json

icml_2012_basic_info = [('booktitle', 'Proceedings of the 29th International Conference on Machine Learning (ICML-12)'), ('series', "ICML '12"), ('year', '2012'), ('location', 'Edinburgh, Scotland, GB'), ('isbn', '978-1-4503-1285-1'), ('month', 'July'), ('publisher', 'Omnipress'), ('address', 'New York, NY, USA')]

def get_icml_2012_bib():
    bib = BibliographyData()
    baseUrl = "https://icml.cc/imls/conferences/2012/" 
    index = baseUrl + "papers.1.html"
    bibfileTemplate = baseUrl + "papers/bibfiles/{}.bib"
    pdfUrlTemplate = baseUrl + "papers/{}.pdf"

    page = get_and_parse(index)
    for paper in tqdm(page.find_all("div", id=re.compile("paper-"))):
        pId = paper['id']
        bibfile = requests.get(bibfileTemplate.format(pId))
        if bibfile.ok:
            bibEntry = parse_string(StringIO(bibfile.text))
        else:
            fields = dict(icml_2012_basic_info + [('title', paper.h2.text)]) 
            persons = {'author': parse_authors(paper)}
            bibEntry = Entry("InProceedings", fields=fields, persons=persons, collection=bib)
        bibEntry.fields['abstract'] = parse_abstract(paper)
        bibEntry.fields['url'] = pdfUrlTemplate.format(pId)
        bibEntry.fields['pdf'] = pdfUrlTemplate.format(pId)
        key = generate_key(bibEntry)
        bib.entries[key] = bibEntry
    return bib

def get_icml_2010_bib():
    "The proceedings bib is available, but the URLs need to be updated and abstracts need to be added"
    metadata = {}
    page = get_and_parse("https://icml.cc/imls/conferences/2010/abstracts.html")
    ids = [a.attrs['name'] for a in page.table.find_all(lambda t: re.search('\d+', t.attrs.get('name', '')))]
    abstracts = [t.text for t in page.table.find_all(class_='abstracts')]
    urls = ["https://icml.cc/imls/conferences/2010/" + a.attrs['href'] 
            for a in page.table.find_all(lambda t: t.text == "Full Paper")]
    metadata = {id: {"abstract": ab, "url": url} for id, ab, url in zip(ids, abstracts,urls)}

    resp = requests.get("https://icml.cc/imls/conferences/2010/papers/icml2010.bib")
    bib = parse_string(resp.text, 'bibtex')
    for e in bib.entries.values():
        match = re.search("(\d+)\.pdf", e.fields['URL'])
        if not match: 
            continue
        eId = match.group(1)
        e.fields['URL'] = metadata[eId]['url']
        e.fields['abstract'] = metadata[eId]['abstract']
    return bib

def get_icml_2009_bib():
    "The proceedings bib is available, but the URLs need to be updated and abstracts need to be added"
    metadata = {}
    page = get_and_parse("https://icml.cc/imls/conferences/2009/abstracts.html")
    idTags = page.find_all(lambda t: re.match("paper ID: \d+", t.text))
    ids = [re.search("paper ID: (\d+)", t.text).group(1) for t in idTags]
    titles = [t.find_previous_sibling('h3').text for t in idTags]
    abstracts = [t.find_next_sibling('p').text for t in idTags]
    urls = ["https://icml.cc/imls/conferences/2009/" + a.attrs['href'] 
            for a in page.find_all(lambda t: t.text == "Full paper")]
    metadata = {t: {"abstract": ab, "url": url} for t, ab, url in zip(titles, abstracts,urls)}

    resp = requests.get("https://icml.cc/imls/conferences/2009/icml09.bib")
    bib = parse_string(resp.text, 'bibtex')
    for e in tqdm(bib.entries.values()):
        title, score = process.extractOne(e.fields['title'], metadata.keys())
        if score < 90: print("Questionable match: {} -> {}".format(e.fields['title'], title))
        meta = metadata[title]
        e.fields['url'] = meta['url']
        e.fields['pdf'] = meta['url']
        e.fields['abstract'] = meta['abstract']
    return bib

def get_icml_2008_bib():
    metadata = {}
    page = get_and_parse("https://icml.cc/imls/conferences/2008/abstracts.shtml.html")
    idTags = page.find_all(lambda t: re.match("paper ID: \d+", t.text))
    ids = [re.search("paper ID: (\d+)", t.text).group(1) for t in idTags]
    titles = [t.find_next_sibling('h3').text for t in idTags]
    abstracts = [t.find_next_sibling('p').find_next_sibling('p').text for t in idTags]
    urls = ["https://icml.cc/imls/conferences/2009/" + a.attrs['href'] 
            for a in page.find_all(lambda t: t.text == "Full paper")]
    metadata = {t: {"abstract": ab, "url": url} for t, ab, url in zip(titles, abstracts,urls)}

    # There are errors in the file: repeated bibliograhpy entries
    # So download and then manually edit
    #resp = requests.get("https://icml.cc/imls/conferences/2008/ICML2008.bib")
    #bib = parse_string(resp.text, 'bibtex')
    bib = parse_file("ICML2008.bib")
    
    for e in tqdm(bib.entries.values()):
        title, score = process.extractOne(e.fields.get('title', "NO TITLE"), metadata.keys())
        if score < 90: 
            print("Questionable match: {} -> {}".format(e.fields.get('title', "NO TITLE"), title))
            print(e)
        meta = metadata[title]
        e.fields['url'] = meta['url']
        e.fields['pdf'] = meta['url']
        e.fields['abstract'] = meta['abstract']
    return bib

def get_icml_2007_bib(cache=False):
    CACHE = "ICML_2007_CACHE.json"
    if cache:
        with open(CACHE) as cachefile:
            metadata = json.load(cachefile)
    else:
        baseUrl = "https://icml.cc/imls/conferences/2007/"
        page = get_and_parse("https://icml.cc/imls/conferences/2007/proceedings.html") 
        papers = page.find_all(class_="section")[1:]
        titles = [p.h2.text for p in papers]
        abstractUrls = [baseUrl + a.attrs['href'] for a in page.find_all(lambda t: t.text == "[Abstract]")]
        abstracts = []
        for u in tqdm(abstractUrls):
            abstractPage = get_and_parse(u)
            abstracts.append(abstractPage.table.find_all('tr')[-1].text.strip())
        urls = [baseUrl + a.attrs['href'] for a in page.find_all(lambda t: t.text == "[Paper]")]
        metadata = {t: {"abstract": ab, "url": url} for t, ab, url in zip(titles, abstracts,urls)}
        with open(CACHE, 'w') as cachefile:
            json.dump(metadata, cachefile)
    
    #bib = parse_remote_bib("https://icml.cc/imls/conferences/2007/icml2007.bib")
    bib = parse_file("icml2007.bib")
    for e in tqdm(bib.entries.values()):
        title, score = process.extractOne(e.fields.get('title', "NO TITLE"), metadata.keys())
        if score < 90: 
            print("Questionable match: {} -> {}".format(e.fields.get('title', "NO TITLE"), title))
            print(e)
        meta = metadata[title]
        e.fields['url'] = meta['url']
        e.fields['pdf'] = meta['url']
        e.fields['abstract'] = meta['abstract']
    return bib

def get_icml_2006_bib():
    basicInfo = {
        "booktitle": "Proceedings of the 23rd International Conference on Machine Learning",
        "series": "ICML '06",
        "year": "2006",
        "isbn": "1-59593-383-2",
        "location": "Pittsburgh, Pennsylvania, USA",
        "publisher": "ACM",
        "address": "New York, NY, USA"
    }
    ACMProceedings("temp/acm_page_2006.html", basicInfo).bib.to_file("bibliographies/icml_2006.bib")

def get_icml_2005_bib():
    basicInfo = {
        "booktitle": "Proceedings of the 22nd international conference on Machine learning",
        "series": "ICML '05",
        "year": "2005",
        "isbn": "1-59593-180-5 ",
        "location": "Bonn, Germany",
        "publisher": "ACM",
        "address": "New York, NY, USA"
    }
    ACMProceedings("temp/acm_page_2005.html", basicInfo).bib.to_file("bibliographies/icml_2005.bib")

def get_icml_2004_bib():
    from proceedings import ICML2004Proceedings
    ICML2004Proceedings().bib.to_file("bibliographies/icml_2004.bib")

def get_icml_2003_bib():
    from proceedings import ICML2003Proceedings
    ICML2003Proceedings().bib.to_file("bibliographies/icml_2003.bib")

def get_icml_2002_bib():
    from proceedings import ICML2002Proceedings
    ICML2002Proceedings().bib.to_file("bibliographies/icml_2002.bib")

#get_icml_2012_bib().to_file("icml_2012.bib")
#get_icml_2010_bib().to_file("icml_2010.bib")
#get_icml_2009_bib().to_file("icml_2009.bib")
#get_icml_2008_bib().to_file("icml_2008.bib")
#get_icml_2007_bib(True).to_file("icml_2007.bib")
#get_icml_2006_bib()
#get_icml_2005_bib()
#get_icml_2004_bib()    
#get_icml_2003_bib()    
#get_icml_2002_bib()



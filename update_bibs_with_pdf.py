# We have some bibliographies which lack links to PDFs, despite the availability of the PDFs on the
# conference website. 

from pathlib import Path
from pybtex.database import BibliographyData, Entry, parse_string, Person, parse_file
from bs4.element import NavigableString
from fuzzywuzzy import process
import requests_cache
from helpers import *
import re
import shutil
import logging

class ICML2005:
    """
    Open ICML2005 bibliography, scrape its webpage, fuzzy-match the bibs to PDF links, and save the bibliography.
    """

    bibfile = "../results/bibliographies/icml/icml_2005.bib"
    confbaseurl = "https://icml.cc/Conferences/2005/"
    confurl = "https://icml.cc/Conferences/2005/proceedings.html"
    MIN_FUZZ = 90

    def update(self):
        self.bib = parse_file(self.bibfile)
        page = get_and_parse(self.confurl)
        links = page.find_all('a')
        self.pdflinks = {a.text : (self.confbaseurl + a.attrs['href']) for a in links if a.text}
        for entry in self.bib.entries.values():
            entry.fields['pdf'] = entry.fields['url'] = self.fuzzy_lookup(entry.fields['title'])

    def save(self):
        self.bib.to_file(self.bibfile)

    def fuzzy_lookup(self, title):
        matchedTitle, score = process.extractOne(title, self.pdflinks.keys())
        if score >= self.MIN_FUZZ:
            print("{} -> {}".format(title, self.pdflinks[matchedTitle]))
            return self.pdflinks[matchedTitle] 
        else:
            print("WARNING: BAD MATCH FOR {}: {} (score {})".format(title, matchedTitle, score))

class ICML2010:
    bibfile = "../results/bibliographies/icml/icml_2010.bib"
    def update(self):
        self.bib = parse_file(self.bibfile)
        for entry in self.bib.entries.values():
            entry.fields['pdf'] = entry.fields['url']
    
    def save(self):
        self.bib.to_file(self.bibfile)

class ICML2012:
    bibfile = "../results/bibliographies/icml/icml_2012.bib"
    urlpattern = "https://icml.cc/imls/conferences/2012/papers/{}.pdf"
    def update(self):
        self.bib = parse_file(self.bibfile)
        for entry in self.bib.entries.values():
            match = re.search('paper-(\d+)', entry.fields['url'])
            if not match:
                print("NO MATCH: {}".format(entry.fields['url']))
            num = match.group(1)
            entry.fields['pdf'] = entry.fields['url'] = self.urlpattern.format(num)
    
    def save(self):
        self.bib.to_file(self.bibfile)

class ICML2011:
    """
        Manually downloaded all the PDFs.
        Move the PDF into the proper place using the entry's key
    """
    bibfile = "../results/bibliographies/icml/icml_2011.bib"
    pdfcache = "/Users/chris/Downloads/icml2011proceedings"
    pdfpath = Path("../results/pdfs/icml/")

    def update(self):
        self.bib = parse_file(self.bibfile)
        self.cache = Path(self.pdfcache)
        def key(path):
            return int(re.match('^(\d+)_', path.name).group(1))
        self.cachefiles = list(sorted(self.cache.glob('*.pdf'), key=key))
        for entry, path in zip(self.bib.entries.values(), self.cachefiles):
            newpath = self.pdfpath / '{}.pdf'.format(entry.key)
            print("[{}] {} -> {}".format(entry.key, path, newpath))
            shutil.move(path, newpath)

class ICML2006:
    """
        Manually downloaded all the PDFs.
        Move the PDF into the proper place using the entry's key
    """
    bibfile = "../results/bibliographies/icml/icml_2006.bib"
    confurl = "https://icml.cc/Conferences/2006/proceedings.html"
    pdfcache = "/Users/chris/Desktop/icml2006"
    pdfpath = Path("../results/pdfs/icml/")
    MIN_FUZZ = 90

    def update(self):
        self.bib = parse_file(self.bibfile)
        self.cache = Path(self.pdfcache)
        self.cachefiles = list(map(str, self.cache.glob('*.pdf')))
        for entry in self.bib.entries.values():
            path = self.get_pdf_path(entry)
            newpath = self.pdfpath / '{}.pdf'.format(entry.key)
            print('[{}] {}: {} -> {}'.format(entry.key, entry.fields['title'], path, newpath))
            shutil.move(path, newpath)

    def get_pdf_path(self, entry):
        page = entry.fields['pages'].split('-')[0]
        for f in self.cachefiles:
            if 'p{}-'.format(page) in str(f):
                return self.cache / f

    def save(self):
        self.bib.to_file(self.bibfile)

class PdfMover:
    """
    Imagine you've got a folder full of PDFs corresponding to bib entries, and they're titled with
    the titles of the papers. That's where this class comes in!
    """
    bibfilePattern = "../results/bibliographies/{}/{}_{}.bib"
    pdfcache = "/Users/chris/Downloads/{}_{}"
    pdfpath = "../results/pdfs/{}/"
    MIN_FUZZ = 83

    def __init__(self, year, venue, log, filejunk=None):
        self.log = log
        self.bibfile = self.bibfilePattern.format(venue, venue, year)
        self.bib = parse_file(self.bibfile.format(venue, venue, year))
        self.cache = Path(self.pdfcache.format(venue, year))
        self.pdfdir = Path(self.pdfpath.format(venue))
        self.cachefiles = self.cache.glob('*.pdf')
        self.filejunk = filejunk
        self.cachelookup = {self.clean_filename(fn.name) : self.cache / fn for fn in self.cachefiles}

    def update(self):
        updates = []
        for entry in self.bib.entries.values():
            newkey = self.clean_dblp_key(entry)
            if newkey:
                updates.append([entry.key, newkey, entry])
                self.log.debug("[{}] -> [{}]".format(entry.key, newkey))
        for oldkey, newkey, entry in updates:
            entry.key = newkey
            self.bib.entries[newkey] = entry
            del self.bib.entries.__dict__['_dict'][oldkey.lower()]

    def move(self, dryrun=False):
        for entry in self.bib.entries.values():
            newpath = self.pdfdir / "{}.pdf".format(entry.key)
            if newpath.exists():
                self.log.info("[{}] Not moving; already there.".format(entry.key))
                continue 
            path = self.fuzzy_lookup(entry)
            if not path:
                continue
            if not path.exists():
                self.log.warning("[{}] Can't move: file does not exist: {}".format(entry.key, path))
                continue 
            if dryrun:
                self.log.info("[{}] would move {} -> {}".format(entry.key, path, newpath))
            else:
                self.log.info("[{}] moved {} -> {}".format(entry.key, path, newpath))
                shutil.move(path, newpath)

    def fuzzy_lookup(self, entry):
        title = entry.fields['title']
        cleanTitle = title.lower()
        matchedTitle, score = process.extractOne(cleanTitle, self.cachelookup.keys())
        if score < self.MIN_FUZZ:
            self.log.warning("[{}] BAD MATCH FOR {}: {} (score {})".format(entry.key, title, matchedTitle, score))
        return self.cachelookup[matchedTitle] 

    def clean_filename(self, fn):
        "Turns An_efficient-apprhach-To-nonlinear_whatever.pdf into space-separated words"
        if self.filejunk:
            try:
                fn = fn[:fn.index(self.filejunk)]
            except ValueError:
                pass
        return ' '.join(map(str.lower, re.findall('[A-Za-z]+', fn)))


log = get_logger(__file__, 'bib_updates.log', logging.DEBUG)
for year in range(1990, 1996):
    print('='*80)
    print(year)
    print('='*80)
    junk = "{}_Machine-Learning".format(year)
    m = PdfMover(year, 'icml', log, filejunk=junk).move(dryrun=False)







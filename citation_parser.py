from pybtex.database import BibliographyData, Entry, parse_string, Person, parse_file
from bs4 import BeautifulSoup
import requests
import re
from helpers import *
import logging
import sys

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format="[%(levelname)s] %(message)s")

class BaseCitationParser:
    """
    Generalized parser class to extract citation fields. 
    Consider this a pre-parse, ready to be input into pybtex.database.Entry,
    which does further parsing.
    """
    def __init__(self, log=None):
        self.log = log or logging
    
    def expect(self, prop, val, strip=True):
        if val and (not strip or val.strip()):
            self.log.debug("{}: {}".format(prop, val.strip() if strip else val))
        else:
            self.log.warn("No {} found".format(prop))

    def parse_title(self, title):
        self.expect("title", title)
        return title.strip()

    def parse_people(self, people):
        peopleDict = {}
        authors = self.parse_authors(people)
        if any(authors):
            peopleDict['author'] = authors
        self.expect("people", peopleDict, strip=False)
        return peopleDict

    def parse_authors(self, people):
        self.expect("authors", people)
        return authors

    def parse_abstract(self, abstract):
        self.expect("abstract", abstract)
        return abstract.strip()

    def parse_pages(self, pages):
        "Expecting something like 'pages 1-10'"
        match = re.search("(\d+)\s*-\s*(\d+)", pages)
        p =  "{}-{}".format(match.group(1), match.group(2)) if match else None
        self.expect("pages", p)
        return p

    def parse_doi(self, doi):
        self.expect("doi", doi)
        return doi

    def parse_url(self, url):
        print("URL: {}".format(url))
        self.expect("url", url)
        return url

class ACMProceedingsParser(BaseCitationParser):
    def parse_authors(self, authors):
        return [Person(a.strip()) for a in authors.split(",")]

    def parse_doi(self, doi):
        match = re.search("doi>\s*([\w\d\/\.]+)", doi)
        result = match.group(1) if match else None
        self.expect("doi", result)
        return result

    def parse_abstract(self, abstract):
        a = abstract.strip()[:-8] if abstract else ""
        try:
            a = a[a.index("...")+4:]
        except ValueError:
            pass
        self.expect("abstract", a)
        return a

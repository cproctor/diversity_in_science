import requests
from pathlib import Path
urlTemplate = "https://dblp.uni-trier.de/search/publ/api?q=toc%3Adb%2Fconf%2Faaai%2Faaai{}.bht%3A&h=1000&format=bib0&rd=1a"
fileTemplate = "../results/bibliographies/aaai/aaai_{}.bib"



def get_year(year, i=None):
    urlYear = year if year >= 2000 else str(year)[2:]
    if i:
        urlYear = "{}-{}".format(str(urlYear), i)
    url = urlTemplate.format(urlYear)
    f = Path(fileTemplate.format(year))
    if f.exists():
        print("Skipping {}, already done".format(year))
        return True
    print("Trying with {}, {}".format(year, urlYear))
    resp = requests.get(url)
    if not resp.ok or not resp.text:
        print("ERROR fetching bib for {}".format(year))
        return False
    with open(f, 'w') as outf:
        outf.write(resp.text)
    print("SUCCESS writing bib for {}".format(year))
    return True

for year in range(1980, 2019):
    for i in range(3):
        if get_year(year, i): break

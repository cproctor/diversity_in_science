# Sherlock 

This document contains my notes on deploying and running this task pipeline to Sherlock.

## Deployment

- Clone the source repo
  - Generate new keys: `ssh-keygen -t rsa`
  - Add public key to bitbucket repo
  - Clone the repo: `git clone git@bitbucket.org:cproctor/diversity_in_science.git`

- Configure python3
  - Enable python3: `ml python/3.6.1`
  - Install dependencies: `pip3 install --user -r requirements.txt`

- Build libpostal, which is a c dependency of the python package
  - Follow compilation instructions at https://github.com/openvenues/pypostal
  - I got stuck on this, so I just ran the relevant task locally and scp'ed the results to sherlock

- Create storage locations for inputs and outputs
  - Update constants in `diversity_in_science/tasks.py` 

- Transfer source files (corpora, bibliographies, labeled author files, etc.)
  - zip bibliographies and texts
        tar -czf bibs.gz bibliographies/
        scp bibs.gz texts.gz authors/authors_from_text.handlabeled.csv cproctor@dtn.sherlock.stanford.edu
  - On sherlock, unzip: `tar -xvzf bibs.gz`

- Do a dry run with --luigi-deps-tree

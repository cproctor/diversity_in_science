#!/bin/bash
#SBATCH --job-name=diversity_in_science_v3
#SBATCH --time=10:00:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=32G

srun /home/users/cproctor/diversity_in_science/sherlock_jobs/run_v3.sh


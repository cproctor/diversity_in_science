# Let's start simple. AN LDA topic model of the corpora.
from itertools import islice, count
import nltk
from gensim.models.phrases import Phrases, Phraser
from gensim.corpora.dictionary import Dictionary
from gensim.corpora import MmCorpus
from gensim.models.ldamulticore import LdaMulticore
from nltk.corpus import stopwords
import string
from pathlib import Path
from tqdm import tqdm
import pandas as pd

CORPUS_SIZE = 5830
VOCAB_SIZE = 20000
NUM_TOPICS = 100
LDA_INCREMENT = 500

WORDS_PER_TOPIC = 100

PHRASER_FILE = "phraser"
DICTIONARY_FILE = "dict"
BOW_FILE = "bags_of_words"
MODEL_FILE = "model"
DOC_TOPICS_FILE = "doc_topics.csv"
TOPIC_WORDS_FILE = "topic_words.csv"

def clean():
    "delete all cached results, start fresh"
    for f in [
            PHRASER_FILE, 
            DICTIONARY_FILE, 
            BOW_FILE, 
            MODEL_FILE,
            DOC_TOPICS_FILE,
    ]:
        Path(f).unlink()

def get_corpus():
    "returns an iterator over corpus"
    corpus = islice(open("corpus.txt"), CORPUS_SIZE) if CORPUS_SIZE else open("corpus.txt")
    corpus = map(lambda sentence: nltk.word_tokenize(sentence.lower()), corpus)
    corpus = map(lambda sentence: [w for w in sentence if w not in string.punctuation], corpus)
    return tqdm(corpus, total=CORPUS_SIZE) if CORPUS_SIZE else tqdm(corpus)

def get_phraser():
    if Path(PHRASER_FILE).exists():
        print("Loading phraser")
        return Phraser(Phrases.load(PHRASER_FILE))
    else:
        print("Building phraser")
        phrases = Phrases(get_corpus())
        phrases.save(PHRASER_FILE)
        return Phraser(phrases)

def get_dictionary():
    if Path(DICTIONARY_FILE).exists():
        print("Loading dictionary")
        return Dictionary.load(DICTIONARY_FILE)
    else:
        phraser = get_phraser()
        print("Building dictionary")
        dictionary = Dictionary(phraser[get_corpus()], prune_at=VOCAB_SIZE)
        dictionary.save(DICTIONARY_FILE)
        return dictionary

def get_bags():
    if Path(BOW_FILE).exists():
        print("Loading bags of words")
        return MmCorpus(BOW_FILE)
    else:
        dictionary = get_dictionary()
        print("Building bags of words")
        bags = [dictionary.doc2bow(doc) for doc in get_corpus()]
        MmCorpus.serialize(BOW_FILE, bags)
        return bags

def get_last_iter(base_file_name, increment):
    "Looks through the directory for files called base.NUM, for incremental training"
    last = None
    for i in count():
        if i == 0: continue
        if not Path(base_file_name + '.{}'.format(i * increment)).exists():
            return last
        last = i

def get_model():
    if Path(MODEL_FILE).exists():
        print("Loading model")
        return LdaMulticore.load(MODEL_FILE)
    else:
        dictionary = get_dictionary()
        bags = get_bags()
        print("Building model")
        model = LdaMulticore(bags, num_topics=NUM_TOPICS, workers=3, id2word=dictionary)
        model.save(MODEL_FILE)
        return model

def get_topic_words(model=None):
    if Path(TOPIC_WORDS_FILE).exists():
        print("Loading topic words")
        return pd.read_csv(TOPIC_WORDS_FILE)
    else:
        model = model or get_model()
        topics = model.show_topics(num_topics=NUM_TOPICS, num_words=WORDS_PER_TOPIC, formatted=False)
        df = pd.DataFrame([[w for w, p in words] for topicId, words in topics], 
                index=[topicId for topicId, words in topics])
        df.to_csv(TOPIC_WORDS_FILE)
        return df

def get_doc_topics(model=None):
    if Path(DOC_TOPICS_FILE).exists():
        print("Loading document topics")
        return pd.read_csv(DOC_TOPICS_FILE)
    else:
        model = model or get_model()
        print("Calculating doc topics")
        df = pd.DataFrame([model[bag] for bag in get_bags()])
        df.to_csv(DOC_TOPICS_FILE)
        return df

# So what do I want?
# 1. Let's see the topics
# 2. Create a topic distribution over years
# 3. Let's see topic clusers (topic correlations)
model = get_model()
get_topic_words(model)
get_doc_topics(model)





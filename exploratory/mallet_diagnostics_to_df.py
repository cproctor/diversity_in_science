# Mallet outputs an XML diagnostics file. Want it as a tidy dataframe so that
# we can incorporate it into the python/pandas workflow.

import pandas as pd
from xml.etree.ElementTree import ElementTree

def get_topic_diagnostics(diagnosticsFile, label):
    model = ElementTree(file=diagnosticsFile).getroot()
    topics = []
    for topic in model.getchildren():
        td = {k: float(v) for k, v in topic.items()}
        td['model'] = label
        topics.append(td)
    return pd.DataFrame(topics)
    
if __name__ == '__main__':
    topics = get_topic_diagnostics('d10.xml', 10)
    print(topics)
    

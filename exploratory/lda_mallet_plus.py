from gensim.models.wrappers.ldamallet import LdaMallet
from gensim.utils import check_output
import pandas as pd
from xml.etree.ElementTree import ElementTree
import logging

logger = logging.getLogger(__name__)

class LdaMalletPlus(LdaMallet):

    def fdiagnostics(self):
        return self.prefix + 'diagnostics.xml'

    def fevaluator(self):
        return self.prefix + 'evaluator.mallet'

    def fprob(self):
        return self.prefix + 'held_out_prob'

    def train(self, corpus):
        """Train Mallet LDA. This differs from the base gensim version in that it also saves a 
        and a model evaluator for held-out log-likelihood

        Parameters
        ----------
        corpus : iterable of iterable of (int, int)
            Corpus in BoW format

        """
        self.convert_input(corpus, infer=False)
        cmd = self.mallet_path + ' train-topics --input %s --num-topics %s  --alpha %s --optimize-interval %s '\
            '--num-threads %s --output-state %s --output-doc-topics %s --output-topic-keys %s '\
            '--num-iterations %s --inferencer-filename %s --doc-topics-threshold %s --diagnostics-file %s '\
            '--evaluator-filename %s'
        cmd = cmd % (
            self.fcorpusmallet(), self.num_topics, self.alpha, self.optimize_interval,
            self.workers, self.fstate(), self.fdoctopics(), self.ftopickeys(), self.iterations,
            self.finferencer(), self.topic_threshold, self.fdiagnostics(), self.fevaluator()
        )
        # NOTE "--keep-sequence-bigrams" / "--use-ngrams true" poorer results + runs out of memory
        logger.info("training MALLET LDA with %s", cmd)
        check_output(args=cmd, shell=True)
        self.word_topics = self.load_word_topics()
        # NOTE - we are still keeping the wordtopics variable to not break backward compatibility.
        # word_topics has replaced wordtopics throughout the code;
        # wordtopics just stores the values of word_topics when train is called.
        self.wordtopics = self.word_topics

    def get_diagnostics(self):
        """
        Returns a pandas dataframe of topic statistics, including coherence and exclusivity
        """
        model = ElementTree(file=self.fdiagnostics()).getroot()
        topics = []
        for topic in model:
            td = {k: float(v) for k, v in topic.items()}
            td['num_topics'] = self.num_topics
            topics.append(td)
        return pd.DataFrame(topics)

    def held_out_log_likelihood(self, held_out_corpus):
        """
        Computes the log likelihood of a corpus. (Intended use is with a held-out subset)

        Parameters
        ----------
        `held_out_corpus` should be an iterable of bags of words
        """
        self.convert_input(held_out_corpus, infer=True) # It's not for inference, but works ok...
        cmd = self.mallet_path + ' evaluate-topics --evaluator %s --input %s --output-prob %s'
        cmd = cmd % (self.fevaluator(), self.fcorpusmallet() + '.infer', self.fprob())
        logger.info("evaluating MALLET LDA with %s", cmd)
        check_output(args=cmd, shell=True)
        with open(self.fprob()) as fprob:
            return float(fprob.read())

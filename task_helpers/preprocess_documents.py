import numpy as np
import pandas as pd
import re
import settings
from messyresearch.segmentation import split_preamble, split_references, strip_preamble_and_references
from gensim.parsing.preprocessing import preprocess_documents, preprocess_string, strip_tags, strip_multiple_whitespaces, strip_non_alphanum, remove_stopwords, strip_short, stem_text, strip_numeric
from pathlib import Path

def getText(paper):
    if not paper.hastext:
        return None
    with open(paper.textfile) as txt:
        if self.strip_preamble_and_references:
            return strip_preamble_and_references(txt.read())
        else:
            return txt.read()

def remove_extra_stopwords(s):
    for sw in settings.EXTRA_STOPWORDS:
        s = re.sub(sw, '', s)
    return s

FILTERS = [
    strip_tags,    
    strip_non_alphanum,
    strip_numeric,
    remove_stopwords, 
    remove_extra_stopwords,
    strip_short,
    strip_multiple_whitespaces,
    stem_text
]

def preprocess_documents(df, split_docs_in_thirds=False, strip_junk=False):
    """
    Read text from papers df. Then Filter, transform, and shuffle.
    """
    np.random.seed(0)

    df = df[df.hastext].copy()
    df['text'] = df.apply(lambda doc: Path(doc.textfile).read_text(), axis=1)
    if strip_junk:
        df['text'] = df.apply(lambda doc: strip_preamble_and_references(doc.text), axis=1)
    df['tokens'] = df.apply(lambda doc: preprocess_string(doc.text, FILTERS), axis=1)

    if split_docs_in_thirds:
        dfs = [df.copy() for _ in range(3)]
        for i, df_ in enumerate(dfs):
            def get_partial_tokens(tokens):
                third = len(tokens) // 3
                return tokens[i * third : (i+1) * third]
            df_['part'] = i
            df_['tokens'] = df_.apply(lambda doc: get_partial_tokens(doc.tokens), axis=1)
        df = pd.concat(dfs)

    df = df[df.tokens.apply(lambda t: any(t))]
    df = df.sample(frac=1)
    df = df.rename_axis("original_index").reset_index()
    return df

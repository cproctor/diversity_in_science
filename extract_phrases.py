import subprocess
from itertools import chain
from pathlib import Path
import re
import logging
from helpers import get_logger
from os.path import dirname, abspath
from argparse import ArgumentParser

if __name__ == '__main__':
    logLevels = {
        "debug": logging.DEBUG,
        "info": logging.INFO,
        "warning": logging.WARNING,
        "error": logging.ERROR
    }
    HERE = Path(dirname(abspath(__file__)))
    ICML_TEXT_DIR = HERE.parent / "results" / "text" / "icml"
    NIPS_TEXT_DIR = HERE.parent / "results" / "text" / "nips"
    AUTOPHRASE_DIR = HERE.parent / "AutoPhrase"
    AUTOPHRASE_CORPUS = 'corpus.txt'
    LOG_FILE = HERE.parent / "logs" / "extract_phrases.log"
    parser = ArgumentParser("Extract authors from pdf text into a CSV")
    parser.add_argument("-t", "--text_dirs", nargs='*', default=[ICML_TEXT_DIR, NIPS_TEXT_DIR],
            help="Directories containing bibliographies to parse")
    parser.add_argument('-a', '--autophrase_dir', default=AUTOPHRASE_DIR,
            help="Directory containing autophrase repo")
    parser.add_argument('-l', '--log_file', default=str(LOG_FILE), 
            help="File to log to")
    parser.add_argument('-n', '--num_bibs', type=int,
            help="Limit to n bibliographies (for testing)")
    parser.add_argument('-e', '--log_level', choices=logLevels.keys(), default='debug')
    parser.add_argument('--min_title_ratio', type=int, default=60, 
            help="Minimum Levensheim ratio for matching on title in text")
    parser.add_argument('--min_author_ratio', type=int, default=60, 
            help="Minimum Levensheim ratio for matching on author name in text")
    args = parser.parse_args()

    log = get_logger(__file__, args.log_file, logLevels[args.log_level])
    log.info("Extracting phrases using AutoPhrase")
    log.info("Creating corpus...")
    with open(Path(args.autophrase_dir) / AUTOPHRASE_CORPUS, 'w') as outfile:
        # TODO allow using just parts of papers...
        for text_dir in args.text_dirs:
            for text_file in Path(text_dir).glob("*.txt"):
                with open(text_dir / text_file) as infile:
                    outfile.write(re.sub('\n', '', infile.read()) + '\n')
        log.info("Ready for AutoPhrase")

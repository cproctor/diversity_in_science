import re
from pathlib import Path
from os.path import dirname, abspath
from bibtools.reader import bib_dir_reader
from fuzzywuzzy import fuzz
from itertools import chain, tee
import logging
from postal.parser import parse_address
from argparse import ArgumentParser
from extract_authors_from_bibs import outputChoices, name_cleaner_factory
from helpers import get_logger
from country_converter import CountryConverter
import pandas as pd
import us

HERE = Path(dirname(abspath(__file__)))
BIBS_DIR = HERE.parent / "results" / "bibliographies" / "icml"
TEXT_DIR = HERE.parent / "results" / "text" / "icml"
MATCH_RATIO = 60

def get_path(bibtex_key, dr, ext):
    return Path(dr) / "{}.{}".format(bibtex_key, ext)

def get_preamble(key, textDir, max_lines=50):
    lines = []
    with open(get_path(key, textDir, "txt")) as txt:
        for i, line in enumerate(txt):
            if max_lines and i == max_lines: break
            if re.search("abstract\s*$|introduction\s*$", line, re.IGNORECASE):
                return lines
            lines.append(line)
    return lines

def read_title(preamble, title, min_ratio=60):
    """
    Reads lines in the preamble until text resembling the title has been read.
    Returns the title and the iterator of unconsumed preamble lines
    """
    text = ""
    titleLines = []
    bestRatio = 0
    preambleIterator = iter(preamble)
    for line in preambleIterator:
        text += ' ' + line.strip()
        ratio = fuzz.ratio(title.lower(), text.lower())
        if ratio >= bestRatio: 
            titleLines += [line]
            bestRatio = ratio
        else:
            if bestRatio > min_ratio:
                return text, titleLines, chain([line], preambleIterator)
    if bestRatio > min_ratio:
        return text, titleLines, []
    raise ValueError("Could not read title: {}".format(title))

def person2str(person, lower=False):
    name = " ".join(person.first_names + person.middle_names + person.last_names)
    if lower:
        name = name.lower()
    return name

def read_author_names(preamble, authors, min_ratio=60):
    """
    Reads lines until a non-empty line appears not containing author names.
    Returns authorsFound, authorLines, restLines
    Raises StopIteration when the iterator runs out
    """
    authorLines = []
    authorsFound = []
    preambleIt = iter(preamble)
    for line in preambleIt:
        if not line.strip(): # The line is empty. Keep going.
            authorLines.append(line)
            continue
        authorsInLine = list(filter(lambda a: fuzz.ratio(person2str(a, True), line.strip().lower()) > min_ratio, authors))
        if any(authorsInLine):
            authorLines.append(line)
            for a in authorsInLine:
                if a not in authorsFound: authorsFound.append(a)
        else:
            return authorsFound, authorLines, chain([line], preambleIt)
    return authorsFound, authorLines, []

def has_email_address(text):
    "Roughly tries to parse the text as an email address"
    squashed = re.sub("\s", "", text)
    return re.match(".+[@~].+\.\w+$", squashed)

def read_non_author_lines(preamble, authors, min_ratio=60, reject_email=True):
    """
    Reads lines until an author is found. Returns nonAuthorLines, restLines
    """
    nonAuthorLines = []
    preambleIt = iter(preamble)
    for line in preambleIt:
        authorsInLine = filter(lambda a: fuzz.ratio(person2str(a, True), line.strip().lower()) > min_ratio, authors)
        if any(authorsInLine) or (reject_email and has_email_address(line)):
            return nonAuthorLines, chain([line], preambleIt)
        nonAuthorLines.append(line)
    return nonAuthorLines, []

def read_email_addresses(preamble):
    "Reads lines with anything resembling email addresses"
    emailLines = []
    preambleIt = iter(preamble)
    for line in preambleIt:
        if not has_email_address(line) and line.strip():
            return emailLines, chain([line], preambleIt)
        emailLines.append(line)
    return emailLines, []

def has_next(iterable):
    "Checks whether the iterator has any more"
    it = iter(iterable)
    try:
        return True, chain([next(it)], it)
    except StopIteration:
        return False, it

def get_author_affiliations(entry, textDir, min_title_ratio=60, min_author_ratio=60, log=None):
    """
    Reads the preamble of a paper's fulltext and parses out its components.
    Returns a list of dicts containing author affiliations.
    """
    log = log or logging
    cc = CountryConverter()
    preamble = get_preamble(entry.key, textDir)
    authors = list(filter(lambda a: person2str(a).strip(), entry.persons.get('author', [])))
    results = []

    log.info("="*80)
    log.info("title: {}".format(entry.fields.get('title', '')))
    log.info("authors: {}".format("; ".join(map(person2str, authors))))
    log.info("-"*80)
    title, titleIt, restIt = read_title(preamble, entry.fields.get('title', ''), min_ratio=min_title_ratio)
    for line in titleIt:
        log.info("[TITLE]  " + line.strip())
    moreLines, restIt = has_next(restIt)
    while moreLines:
        authorsFound, authorsIt, restIt = read_author_names(restIt, authors, min_ratio=min_author_ratio)
        for line in authorsIt: log.info("[AUTHOR] " + line.strip())
        emailIt, restIt = read_email_addresses(restIt)
        emailIt, emails1 = tee(emailIt)
        for line in emailIt: log.info("[EMAIL] " + line.strip())
        nonAuthorsIt, restIt = read_non_author_lines(restIt, authors, min_ratio=min_author_ratio)
        nonAuthorsIt, affiliation = tee(nonAuthorsIt)
        for line in nonAuthorsIt: log.info("[AFFIL]  " + line.strip())
        emailIt, restIt = read_email_addresses(restIt)
        emailIt, emails2 = tee(emailIt)
        for line in emailIt: log.info("[EMAIL] " + line.strip())
        if any(authorsFound):
            affiliation = dict(map(reversed, parse_address(" ".join(affiliation))))
            log.info('-' * 80)
            log.info("authors: {}".format("; ".join(map(person2str, authorsFound))))
            log.info("affiliations: {}".format(affiliation))
            for author in authorsFound:
                result = {
                    'pubkey': entry.key,
                    'first': ' '.join(author.first_names),
                    'last': ' '.join(author.last_names),
                    'middle': ' '.join(author.middle_names),
                    'place': affiliation.get('house'),
                    'city': affiliation.get('city'),
                    'state': affiliation.get('state'),
                    'postcode': affiliation.get('postcode'),
                    'country': affiliation.get('country'),
                    'continent': cc.convert(affiliation.get('country'), to='continent', not_found=None)
                }
                if not result.get('country') and result.get('state') and us.states.lookup(result.get('state')):
                    log.info("Inferring country USA from state {}".format(result.get('state')))
                    result['country'] = 'usa'
                if not result.get('country'):
                    match = has_email_address(' '.join(chain(emails1, emails2)))
                    if match:
                        tld = match.group().split('.')[-1].lower()
                        if tld in ['edu', 'mil', 'gov', 'us']:
                            log.info("Inferring country usa from email tld .{}".format(tld))
                            result['country'] = 'usa'
                        if tld not in ['net', 'org', 'com']:
                            log.info("Inferring country {} from email tld".format(tld))
                            result['country'] = tld
                results.append(result)
        moreLines, restIt = has_next(restIt)
    return results

def extract_authors_from_text(bib_dirs, text_dirs, fmt, outfile, num_bibs=None, 
        min_title_ratio=60, min_author_ratio=60, log=None):
    log = log or logging
    log.info("Extracting authors from text")
    results = []
    successes = 0
    errors = 0
    for bib_dir, text_dir in zip(bib_dirs, text_dirs):
        bibs = bib_dir_reader(bib_dir)
        for i, entry in enumerate(bibs):
            try:
                if num_bibs and num_bibs == i: break
                if not get_path(entry.key, text_dir, 'txt').exists(): continue
                results += get_author_affiliations(entry, text_dir, log=log,
                        min_title_ratio=min_title_ratio,
                        min_author_ratio=min_author_ratio)
                successes += 1
            except ValueError as e:
                log.warning("[{}] Error reading paper {}: {}".format(entry.key, entry.fields.get('title'), e))
                errors += 1
    clean = name_cleaner_factory(log=log, output=fmt, catch_errors=True)
    results = [{k:clean(v) if v else v for k, v in result.items()} for result in results]
    cols = ['pubkey', 'first', 'last', 'middle', 'place', 'city', 'state', 'postcode', 'country', 'continent']
    results = pd.DataFrame(results, columns=cols)
    results.to_csv(outfile)
    log.info("{} papers successfully read. {} errors.".format(successes, errors))

# TODO: Update authors when the text is better than the bibtex entry
if __name__ == '__main__':
    logLevels = {
        "debug": logging.DEBUG,
        "info": logging.INFO,
        "warning": logging.WARNING,
        "error": logging.ERROR
    }
    HERE = Path(dirname(abspath(__file__)))
    ICML_BIBS_DIR = HERE.parent / "results" / "bibliographies" / "icml"
    NIPS_BIBS_DIR = HERE.parent / "results" / "bibliographies" / "nips"
    ICML_TEXT_DIR = HERE.parent / "results" / "text" / "icml"
    NIPS_TEXT_DIR = HERE.parent / "results" / "text" / "nips"
    OUTPUT_FILE = HERE.parent / "results" / "authors"/"authors_from_text.csv"
    LOG_FILE = HERE.parent / "logs" / "extract_authors_from_text.log"
    parser = ArgumentParser("Extract authors from pdf text into a CSV")
    parser.add_argument("-b", "--bib_dirs", nargs='*', default=[ICML_BIBS_DIR, NIPS_BIBS_DIR],
            help="Directories containing bibliographies to parse")
    parser.add_argument("-t", "--text_dirs", nargs='*', default=[ICML_TEXT_DIR, NIPS_TEXT_DIR],
            help="Directories containing bibliographies to parse")
    parser.add_argument('-f', '--format', choices=outputChoices, default='ascii',
            help="Format of output text")
    parser.add_argument('-o', '--outfile', default=OUTPUT_FILE, 
            help="File to write csv to")
    parser.add_argument('-l', '--log_file', default=str(LOG_FILE), 
            help="File to log to")
    parser.add_argument('-n', '--num_bibs', type=int,
            help="Limit to n bibliographies (for testing)")
    parser.add_argument('-e', '--log_level', choices=logLevels.keys(), default='debug')
    parser.add_argument('--min_title_ratio', type=int, default=60, 
            help="Minimum Levensheim ratio for matching on title in text")
    parser.add_argument('--min_author_ratio', type=int, default=60, 
            help="Minimum Levensheim ratio for matching on author name in text")
    args = parser.parse_args()

    log = get_logger(__file__, args.log_file, logLevels[args.log_level])
    extract_authors_from_text(args.bib_dirs, args.text_dirs, args.format, args.outfile, 
        args.num_bibs, args.min_title_ratio, args.min_author_ratio, log)

import matplotlib.pyplot as plt

def plot_topics(ids, yearlyTopics):
    f, ax = plt.subplots()
    ax.set_ylabel("topic distribution")
    yearlyTopics[ids].plot(ax=ax)
    labels = list(range(len(ids)))
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0., labels=labels)

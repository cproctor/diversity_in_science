# Copy these settings to settings.py
from pathlib import Path
BASE_PATH = Path(".")
INPUTS = BASE_PATH.parent / 'inputs'
RESULTS = BASE_PATH.parent / 'results'
MALLET_PATH = "bin/mallet"
RSCRIPT_PATH = BASE_PATH / 'R'
EXTRACTING_AUTHORS_FROM_TEXT = True

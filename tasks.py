# Tasks.py
# --------------------------------------------------------------
# Runs full analysis for diversity in machine learning paper
# as a graph of tasks, so that they can be partially regenerated
# as needed. Each task results in file(s) on disk. 
# Remove tasks' output files to force rerun of those tasks. 
# Using the luigi task library.
# Run the beast with `PYTHONPATH='.:exploratory' luigi --module tasks SelectNumberOfTopics --local-scheduler`
# --------------------------------------------------------------

import re
import json
from pathlib import Path
from random import shuffle, seed
import subprocess

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import scipy.special as scsp
from scipy.spatial.distance import jensenshannon
from scipy.stats import norm
from gensim.models.phrases import Phrases, Phraser
from gensim.corpora.dictionary import Dictionary
from gensim.corpora import MmCorpus
from gensim.models.ldamulticore import LdaMulticore
from gensim.models.wrappers import LdaMallet
from gensim.models.coherencemodel import CoherenceModel
from gensim.models.wrappers.ldamallet import malletmodel2ldamodel
from gensim.matutils import corpus2csc
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from tmtoolkit.topicmod import tm_gensim, tm_lda
from tmtoolkit.topicmod.visualize import plot_eval_results
from tmtoolkit.topicmod.evaluate import results_by_parameter
import pyLDAvis 
from pyLDAvis.gensim import prepare

import luigi
from luigi.util import requires
from tqdm import tqdm
from fuzzywuzzy.process import extractOne

from bibtools.reader import bib_dir_reader
from bibtools.utils import entries_to_df
from lda_mallet_plus import LdaMalletPlus
from extract_authors_from_bibs import extract_authors_from_bibs
from task_helpers.preprocess_documents import preprocess_documents
import helpers
from settings import *

# This has heavy and prickly dependencies which I haven't gotten working on Sherlock. 
# So this one only runs locally.
if EXTRACTING_AUTHORS_FROM_TEXT:
    from extract_authors_from_text import extract_authors_from_text


class DiversityInScienceTask(luigi.Task):
    """
    Abstract task for classes which follow. Implements `output_dir`, a helper returning a Path to 
    the output directory. Expects subclasses to have a @requires(SharedParams) or an @inherits(SharedParam)
    decorator so that `self.version` is defined.
    """
    version = luigi.Parameter()
    def output_dir(self):
        return RESULTS / 'tasks' / self.version

class BibDirs(DiversityInScienceTask):
    "Wraps the externally-generated bibliography source files"
    venues = luigi.Parameter()
    def output(self):
        return {venue: luigi.LocalTarget(str(INPUTS / 'bibliographies' / venue)) 
                for venue in self.venues.split(',')} 

class PdfDirs(DiversityInScienceTask):
    "Wraps externally-generated pdf source dirs"
    venues = luigi.Parameter()
    def output(self):
        return {venue: luigi.LocalTarget(str(INPUTS / 'pdfs' / venue)) for venue in self.venues.split(',')}

class TextDirs(DiversityInScienceTask):
    "Wraps externally-generated text source dirs"
    venues = luigi.Parameter()
    def output(self):
        return {venue: luigi.LocalTarget(str(INPUTS / 'text' / venue)) for venue in self.venues.split(',')}

class FatBibsDir(DiversityInScienceTask):
    def output(self):
        return luigi.LocalTarget(str(INPUTS / 'bibliographies' / "fatml")) 

class FatTextDir(DiversityInScienceTask):
    def output(self):
        return luigi.LocalTarget(str(INPUTS / 'text' / "fatml")) 

@requires(BibDirs, PdfDirs, TextDirs)
class GeneratePapersDf(DiversityInScienceTask):
    "Creates a papers DataFrame and merges in metadata"
    count_pdfs = luigi.BoolParameter(default=False)

    def run(self):
        bibDirs, pdfDirs, textDirs = self.input()
        papers = [entries_to_df(bib_dir_reader(bibDir.path)) for bibDir in bibDirs.values()]
        venues = bibDirs.keys()
        for venuePapers, venue in zip(papers, venues):
            venuePapers['venue'] = venue
        relevantColumns = ["author0_first", "author0_last", "key", "title", "year", "venue"]
        df = pd.concat([venuePapers[relevantColumns] for venuePapers in papers], ignore_index=True)
        df = df.dropna(subset=['year'])
        df.index.names = ['doc']
        df['year'] = df.year.astype(int)

        def getPdfFilename(paper):
            if paper.key:
                return Path(pdfDirs[paper.venue].path) / "{}.{}".format(paper.key, "pdf")
        def getTextFilename(paper):
            if paper.key:
                return Path(textDirs[paper.venue].path) / "{}.{}".format(paper.key, "txt")
        def hasPdf(paper):
            return Path(paper.pdffile).exists()
        def hasText(paper):
            return Path(paper.textfile).exists()

        if self.count_pdfs:
            df["pdffile"] = df.apply(getPdfFilename, axis=1)
            df["haspdf"] = df.apply(hasPdf, axis=1)

        df["textfile"] = df.apply(getTextFilename, axis=1)
        df["hastext"] = df.apply(hasText, axis=1)
        df.to_csv(self.output().path)

    def output(self):
        return luigi.LocalTarget(str(self.output_dir() / 'papers.csv'))

@requires(FatBibsDir, FatTextDir)
class GenerateFatPapersDf(DiversityInScienceTask):
    "Creates a papers DataFrame and merges in metadata"

    def run(self):
        fatBibsDirPath, fatTextDirPath = self.input()
        papers = [entries_to_df(bib_dir_reader(fatBibsDirPath.path))]
        venues = ['fatml']
        for venuePapers, venue in zip(papers, venues):
            venuePapers['venue'] = venue
        relevantColumns = ["author0_first", "author0_last", "key", "title", "year", "venue"]
        df = pd.concat([venuePapers[relevantColumns] for venuePapers in papers])
        df = df.dropna(subset=['year'])
        df.index.names = ['doc']
        df['year'] = df.year.astype(int)

        def getTextFilename(paper):
            if paper.key:
                return Path(fatTextDirPath.path) / "{}.{}".format(paper.key, "txt")
        def hasText(paper):
            return Path(paper.textfile).exists()

        df["textfile"] = df.apply(getTextFilename, axis=1)
        df["hastext"] = df.apply(hasText, axis=1)
        df.to_csv(self.output().path)

    def output(self):
        return luigi.LocalTarget(str(self.output_dir() / 'fat_papers.csv'))

@requires(GeneratePapersDf)
class CorpusCompletenessReport(DiversityInScienceTask):
    """
    Reports how much of the corpus we have. 
    Load the resulting csv with `pd.read_csv("corpus_stats.csv", header=[0,1,2], index_col=0)`
    """
    def run(self):
        papersFile = self.input()
        df = pd.read_csv(papersFile.path)
        completeness = df[['year', 'venue', 'hastext']].groupby(['year', 'venue']) \
            .agg({'hastext': 'mean'}).unstack().fillna(0).round(2)
        with self.output()['text'].open('w') as outf:
            outf.write("We have full text of {} out of {} total papers.".format(len(df[df.hastext]), len(df)))
        completeness.to_csv(self.output()['csv'].path)

    def output(self):
        return {
            'csv': luigi.LocalTarget(str(self.output_dir() / 'corpus_stats.csv')),
            'text': luigi.LocalTarget(str(self.output_dir() / 'corpus_stats.txt'))
        }

@requires(GeneratePapersDf)
class EvaluatePaperSplits(DiversityInScienceTask):
    "Reports relative positions of where the preamble and the reference splits occur"
    def run(self):
        df = pd.read_csv(self.input().path)                         
        df = df[df.hastext]
        df['text'] = df.textfile.apply(lambda f: Path(f).read_text())
        df['preamble'] = df.text.apply(lambda t: split_preamble(t, stats=True)['position'])
        df['references'] = df.text.apply(lambda t: split_references(t, stats=True)['position'])
        df['length'] = df.text.str.len()
        df['preamble_percent'] = df['preamble'] / df['length']
        df['references_percent'] = df['references'] / df['length']
        cols = ['length', 'preamble', 'preamble_percent', 'references', 'references_percent']
        df[cols].to_csv(self.output()['csv'].path)

        results = []
        dfp = df[~df.preamble.isnull()]
        results.append("Preamble found: {:.2f}%".format(len(dfp) / len(df)))
        results.append("Preamble percentage: mean {:.2f}, std {:.2f}".format(
                dfp.preamble_percent.mean(), (dfp.preamble_percent.std())))
        results.append("Preamble min/max ids: {}, {}".format(
                dfp.preamble_percent.idxmin(), dfp.preamble_percent.idxmax()))
        dfr = df[~df.references.isnull()]
        results.append("References found: {:.2f}%".format(len(dfr) / len(df)))
        results.append("References percentage: mean {:.2f}, std {:.2f}".format(
                dfr.references_percent.mean(), (dfr.references_percent.std())))
        results.append("References min/max ids: {}, {}".format(
                dfr.references_percent.idxmin(), dfr.references_percent.idxmax()))
        with self.output()['stats'].open('w') as outf:
            outf.write('\n'.join(results))

    def output(self):
        return {
            'csv': luigi.LocalTarget(str(self.output_dir() / 'preamble_reference_splits.csv')),
            'stats':luigi.LocalTarget(str(self.output_dir() / 'preamble_reference_splits.txt')),
        }

class PreprocessDocumentsMixin:
    """
    Read text from papers df. Then filter, transform, and shuffle, and save the output.
    """
    slug = "processed"

    def run(self):
        papersFile, _ = self.input()
        df = pd.read_csv(papersFile.path, index_col='doc')                         
        df = preprocess_documents(df, self.split_docs_in_thirds, self.strip_preamble_and_references)
        df.index.names = ['doc']
        with open(self.output()['docs'].path, 'w') as outfile:
            json.dump(df.tokens.tolist(), outfile)                               
        df.drop(columns=['text', 'tokens']).to_csv(self.output()['df'].path)

    def qualifier(self):
        qual = '' 
        if self.split_docs_in_thirds:
            qual += '_thirds'
        if self.strip_preamble_and_references:
            qual += '_stripped'
        return qual

    def output(self):
        p = (self.slug, self.qualifier())
        return {
            "df": luigi.LocalTarget(str(self.output_dir() / '{}_papers{}.csv'.format(*p))),
            "docs": luigi.LocalTarget(str(self.output_dir() / '{}_documents{}.json'.format(*p)))
        }

@requires(GeneratePapersDf, TextDirs)
class PreprocessDocuments(PreprocessDocumentsMixin, DiversityInScienceTask):
    split_docs_in_thirds = luigi.BoolParameter(default=False)
    strip_preamble_and_references = luigi.BoolParameter(default=False)
    slug = "processed"

@requires(GenerateFatPapersDf, FatTextDir)
class PreprocessFatDocuments(PreprocessDocumentsMixin, DiversityInScienceTask):
    split_docs_in_thirds = luigi.BoolParameter(default=False)
    strip_preamble_and_references = luigi.BoolParameter(default=False)
    slug = "processed_fat"

class BuildCorpusMixin:
    "Converts preprocessed documents into a gensim Corpus and Dictionary"
    slug = ""

    def run(self):
        rawCorpusFile = self.input()['docs']
        with open(rawCorpusFile.path) as infile:
            docs = json.load(infile)
        bigrams = Phraser(Phrases(docs))
        trigrams = Phraser(Phrases(bigrams[docs], min_count=100))
        collocatedCorpus = trigrams[bigrams[docs]]
        dictionary = Dictionary(collocatedCorpus)
        # Note: the dictionary is not compactified. There are possibly all-zero columns in the  
        # doc-word matrix. This shouldn't be a huge issue, just a bit wasteful.
        dictionary.filter_extremes(no_below=self.no_below, no_above=self.no_above, keep_n=self.keep_n)
        print("Dictionary contains {} tokens".format(len(dictionary)))
        bagsOfWords = [dictionary.doc2bow(doc) for doc in collocatedCorpus]
        MmCorpus.serialize(self.output()['corpus'].path, bagsOfWords)
        dictionary.save(self.output()['dictionary'].path)

        corpusMask = [bool(bow) for bow in bagsOfWords]
        df = pd.read_csv(self.input()['df'].path)
        df = df[corpusMask]
        df = df.rename_axis("processed_index").reset_index()
        df.to_csv(self.output()['df'].path)

    def output(self):
        return {
            'dictionary': luigi.LocalTarget(str(self.output_dir() / '{}dictionary'.format(self.slug))),
            'corpus': luigi.LocalTarget(str(self.output_dir() / '{}corpus'.format(self.slug))),
            'df': luigi.LocalTarget(str(self.output_dir() / 'papers_{}corpus.csv'.format(self.slug)))
        }

@requires(PreprocessDocuments)
class BuildCorpus(BuildCorpusMixin, DiversityInScienceTask):
    "Converts preprocessed documents into a gensim Corpus and Dictionary"
    no_below = luigi.IntParameter(default=1)
    no_above = luigi.FloatParameter(default=1.0)
    keep_n = luigi.IntParameter(default=500000)
    slug = ""

@requires(PreprocessFatDocuments)
class BuildFatCorpus(BuildCorpusMixin, DiversityInScienceTask):
    "Converts preprocessed documents into a gensim Corpus and Dictionary"
    no_below = luigi.IntParameter(default=1)
    no_above = luigi.FloatParameter(default=1.0)
    keep_n = luigi.IntParameter(default=500000)
    slug = "fat_"

@requires(BuildCorpus, BuildFatCorpus)
class GetFatWordsOddsRatios(DiversityInScienceTask):
    "Gets odds ratios of words appearing in Fat vs in main corpus"

    def run(self):
        c, fc = self.input()
        cCorpus = corpus2csc(MmCorpus(c['corpus'].path)).T
        fcCorpus = corpus2csc(MmCorpus(fc['corpus'].path)).T
        print('cCorpus {}, fcCorpus {}'.format(cCorpus.shape, fcCorpus.shape))
        cDict = Dictionary.load(c['dictionary'].path)
        fcDict = Dictionary.load(fc['dictionary'].path)

        words = sorted(set(cDict.token2id.keys()).intersection(set(fcDict.token2id.keys())))

        cWordIds = [cDict.token2id[w] for w in words]
        cOdds = cCorpus[:, cWordIds].sum(axis=0) / cCorpus.sum()
        fcWordIds = [fcDict.token2id[w] for w in words]
        fcOdds = fcCorpus[:, fcWordIds].sum(axis=0) / fcCorpus.sum()
        oddsRatios = fcOdds / cOdds

        df = pd.DataFrame({'odds_ratio': np.ravel(oddsRatios)}, index=words) 
        df.to_csv(self.output().path)

    def output(self):
        return luigi.LocalTarget(str(self.output_dir() / 'fat_odds_ratios.csv'))

@requires(BuildCorpus)
class SelectNumberOfTopicsMultifactor(DiversityInScienceTask):
    "Again, trains models for various numbers of topics to find the optimum"

    def run(self):
        bagsOfWords = MmCorpus(self.input()['corpus'].path)
        dictionary = Dictionary.load(self.input()['dictionary'].path)
        const_params = {
            "n_iter": 400
        }
        varying_params = [
            {
                "n_topics": k,
                "alpha": 1.0/k
            } 
            for k in [10, 20, 30, 40, 50, 60, 70, 80, 100, 120, 140, 160, 200, 240, 300]   
        ]
        models = tm_lda.evaluate_topic_models(
            corpus2csc(bagsOfWords, dtype=np.int), 
            varying_params, 
            constant_parameters=const_params, 
            return_models=True, 
        )
        results_by_n_topics = results_by_parameter(models, 'n_topics')
        plot_eval_results(results_by_n_topics)
        plt.savefig(self.output().path)

    def output(self):
        return luigi.LocalTarget(str(self.output_dir() / 'topic_evaluation_multifactor.png'))
        
@requires(BuildCorpus)
class SelectNumberOfTopics(DiversityInScienceTask):
    "Trains models for various numbers of topics to find the optimum"

    def run(self):
        bagsOfWords = MmCorpus(self.input()['corpus'].path)
        dictionary = Dictionary.load(self.input()['dictionary'].path)
        train, test = train_test_split(bagsOfWords, test_size=0.1)

        scores = []
        diagnostics = []
        lls = []
        n_values = range(10, 210, 10)
        for n in n_values:
            model = LdaMalletPlus(MALLET_PATH, corpus=train, num_topics=n, id2word=dictionary, iterations=400)
            diagnostics.append(model.get_diagnostics())
            lls.append(model.held_out_log_likelihood(test))
        ddf = pd.concat(diagnostics)
        llDict = {n:ll for n, ll in zip(n_values, lls)}
        ddf['log_likelihood'] = ddf.num_topics.apply(lambda n: llDict[n])
        ddf.to_csv(self.output()['df'].path)

        f, (ax0, ax1, ax2) = plt.subplots(1,3, figsize=(18,4))
        ddf.groupby('num_topics').mean().coherence.plot(ax=ax0)
        (ddf.groupby('num_topics').mean().exclusivity * ddf.num_topics.unique()).plot(ax=ax1)
        ddf.groupby('num_topics').mean().log_likelihood.plot(ax=ax2)
        ax0.set_ylabel("coherence")
        ax1.set_ylabel("exclusivity")
        ax2.set_ylabel("log_likelihood")
        ax2.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        plt.savefig(self.output()['chart'].path)

    def output(self):
        return {
            'df': luigi.LocalTarget(str(self.output_dir() / 'topic_evaluation.csv')),
            'chart': luigi.LocalTarget(str(self.output_dir() / 'topic_evaluation.png'))
        }

@requires(BuildCorpus)
class SelectNumberOfTopicsLdaTuning(DiversityInScienceTask):
    "Uses R's ldatuning package to select the optimal number of topics"
    def run(self):
        tuner = RSCRIPT_PATH / "find_k.R"
        call = "R_MAX_VSIZE=32000000000 {} {} {}".format(str(tuner), self.input()['corpus'].path, self.output().path)
        print(call)
        subprocess.run(call, shell=True, check=True)

    def output(self):
        return luigi.LocalTarget(str(self.output_dir() / 'optimal_k.png'))

@requires(BuildCorpus)
class BuildModel(DiversityInScienceTask):
    ntopics = luigi.IntParameter(default=70)
    iterations = luigi.IntParameter(default=1000)
    optimize_interval = luigi.IntParameter(default=10)

    def run(self):
        corpusFiles = self.input()
        bagsOfWords = MmCorpus(corpusFiles['corpus'].path)
        dictionary = Dictionary.load(corpusFiles['dictionary'].path)
        model = LdaMalletPlus(MALLET_PATH, corpus=bagsOfWords, id2word=dictionary,
                num_topics=self.ntopics, optimize_interval=self.optimize_interval, 
                iterations=self.iterations)
        model.save(self.output()['model'].path)
        model.get_diagnostics().to_csv(self.output()['stats'].path)

    def output(self):
        return {
            'model': luigi.LocalTarget(str(self.output_dir() / 'lda_model_{}'.format(self.ntopics))),
            'stats': luigi.LocalTarget(str(self.output_dir() / 'lda_model_stats_{}.csv'.format(self.ntopics)))
        }

@requires(SelectNumberOfTopics, BuildModel)
class PlotModelComparison(DiversityInScienceTask):
    "Plots model diagnostics with final model stats overlaid"
    ntopics = luigi.IntParameter(default=70)

    def run(self):
        diagnostics, modelData = self.input()
        model = LdaMalletPlus.load(modelData['model'].path)
        modelStats = pd.read_csv(modelData['stats'].path)
        ddf = pd.read_csv(diagnostics['df'].path)

        f, (ax0, ax1) = plt.subplots(1,2, figsize=(10,4))
        ddf.groupby('num_topics').mean().coherence.plot(ax=ax0)
        ddf.groupby('num_topics').mean().log_likelihood.plot(ax=ax1)
        ax0.set_ylabel("coherence")
        ax1.set_ylabel("log_likelihood")
        ax0.scatter([self.ntopics], [modelStats.coherence.mean()], color='orange')
        ax1.scatter([self.ntopics], [modelStats.coherence.mean()], color='orange')
        plt.savefig(self.output().path)

    def output(self):
        return luigi.LocalTarget(str(self.output_dir() / 'lda_model_topic_evaluation.png'))

@requires(BuildCorpus, BuildModel)
class EstimateTopics(DiversityInScienceTask):
    "Uses the LDA model to estimate topics for the documents"

    def run(self):
        corpus, model = self.input()
        bagsOfWords = MmCorpus(corpus['corpus'].path)
        model = LdaMalletPlus.load(model['model'].path)
        gensimLdaModel = malletmodel2ldamodel(model, iterations=1000)
        topics = pd.DataFrame([dict(gensimLdaModel[bag]) for bag in bagsOfWords]).fillna(0)
        topics.to_csv(self.output().path)

    def output(self):
        return luigi.LocalTarget(str(self.output_dir() / 'topics.csv'))

@requires(BuildCorpus, EstimateTopics)
class GlobalTopicLoads(DiversityInScienceTask):
    """
    There are a few different ways to think about the topic loads over the whole corpus. 
    You could weight each document equally. Any you could normalize years. Here we follow 
    the approach of pyLDAvis, and estimate the proportion of corpus tokens assigned to
    each topic.
    """
    def run(self):
        corpusData, topicsFile = self.input()
        corpus = MmCorpus(corpusData['corpus'].path)
        topics = pd.read_csv(topicsFile.path, index_col=0)
        docLengths = [sum(count for word, count in bow) for bow in corpus]
        freqs = topics.T * docLengths
        probs = freqs.sum(axis=1) / freqs.values.sum()
        probs = probs.sort_values(ascending=False).reset_index()
        probs = probs.rename(columns={'index': 'topic_id', 0:'load'})
        probs.to_csv(self.output().path)

    def output(self):
        return luigi.LocalTarget(str(self.output_dir() / 'topic_loads.csv'))
    
@requires(BuildModel)
class PlotTopicWordLengths(DiversityInScienceTask):

    def run(self):
        stats = pd.read_csv(self.input()['stats'].path)
        sns.scatterplot(x='word-length', y='tokens', data=stats)
        plt.savefig(self.output().path)

    def output(self):
        return luigi.LocalTarget(str(self.output_dir() / 'topic_word_lengths.png'))

class LabeledAuthorSectors(DiversityInScienceTask):
    "Wraps the expected df containing gold standard author sectors"
    def output(self):
        return luigi.LocalTarget(str(INPUTS / 'authors' / 'authors_from_text.handlabeled.csv'))

class AuthorIdentities(DiversityInScienceTask):
    "Wraps the expected df (from Bas) containing estimated author identities"
    def output(self):
        return luigi.LocalTarget(str(INPUTS / 'authors' / 'authors_identities.csv'))

@requires(BibDirs)
class ExtractAuthorsFromBibliographies(DiversityInScienceTask):
    "Reads bibliographies and extracts a DataFrame of authors"

    def run(self):
        extract_authors_from_bibs(
            bib_dirs=[d.path for d in self.input().values()],
            fmt='ascii',
            outfile=self.output().path
        )

    def output(self):
        return luigi.LocalTarget(str(self.output_dir() / 'authors_from_bibs.csv')) 

@requires(BibDirs, TextDirs)
class ExtractAuthorsFromText(DiversityInScienceTask):
    "Reads full text of articles and parses out authors, along with their metadata"

    def run(self):
        bibDirs, textDirs = self.input()
        extract_authors_from_text(
            bib_dirs=[d.path for d in bibDirs.values()],
            text_dirs=[d.path for d in textDirs.values()],
            fmt='ascii',
            outfile=self.output().path,
            log = helpers.get_logger(__file__, "../logs/extract_authors_from_text.log")
        )

    def output(self):
        return luigi.LocalTarget(str(self.output_dir() / 'authors_from_text_with_context.csv'))

@requires(LabeledAuthorSectors, ExtractAuthorsFromBibliographies, ExtractAuthorsFromText)
class AuthorContexts(DiversityInScienceTask):
    """
    Generates author contexts, including country, continent, and sector. These are 
    extracted from the text of the papers, but we actually care about the author names
    from the bibliographies. There will not be an exact match here. The 
    `min_author_fuzz_score` parameter specifies the minimum Levensheim (sp?) distance
    to accept when matching authors across data sources.
    """
    min_author_fuzz_score = luigi.IntParameter(default=80)

    def run(self):
        labeledAuthorsFile, authorsFromBibsFile, authorsFromTextFile = self.input()
        authorsFromBibs = pd.read_csv(authorsFromBibsFile.path, index_col=0)

        labeled = pd.read_csv(labeledAuthorsFile.path)
        labeled = labeled[(labeled.place.notnull()) & (labeled.classification.notnull())]
        nbModel = Pipeline([
            ('vectorizer', CountVectorizer(analyzer='char', ngram_range=(2, 8))),
            ('classifier', MultinomialNB())
        ])
        score = sum([self.evaluate_model(nbModel, labeled) for i in range(10)]) / 10
        pd.DataFrame({'sector_cv_score': [score]}).to_csv(self.output()['stats'].path)
        nbModel.fit(labeled.place, labeled.classification)

        authorsFromText = pd.read_csv(authorsFromTextFile.path, index_col=0)
        hasPlace = authorsFromText.place.notnull()
        authorsFromText.loc[hasPlace, 'sector'] = nbModel.predict(authorsFromText[hasPlace].place)
        authorsFromText['name'] = authorsFromText['first'].str.lower() + ' ' + authorsFromText['last'].str.lower()
        textPubs = authorsFromText.groupby('pubkey')

        def get_author_text_name(author):
            "Given an author from bibtext, tries to find the closest match in authors read from text"
            authorName = str(author['first']).lower() + ' ' + str(author['last']).lower()
            try:
                textPubAuthorNames = textPubs.get_group(author.pubkey).name
                match = extractOne(authorName, textPubAuthorNames, score_cutoff = self.min_author_fuzz_score)
                if match:
                    name, _, _ = match
                    return name
                else:
                    return None
            except KeyError: # This publication key has no text, or no authors were found
                return None

        authorsFromBibs['text_name'] = authorsFromBibs.apply(get_author_text_name, axis=1)
        contexts = authorsFromText[['pubkey', 'name', 'place', 'city', 'state', 'country', 'continent', 'sector']]
        authorsFromBibs = authorsFromBibs.merge(contexts, how='left', left_on=['pubkey', 'text_name'], 
                right_on=['pubkey', 'name'])
        authorsFromBibs.to_csv(self.output()['df'].path)

    def evaluate_model(self, model, labeled):
        "Train on whatever labeled data we have; see how well it does"
        train, test = train_test_split(labeled, test_size = 0.2)
        model.fit(train.place, train.classification)
        return model.score(test.place, test.classification)

    def output(self):
        return {
            'df': luigi.LocalTarget(str(self.output_dir() / 'authors_from_bibs_with_context.csv')),
            'stats': luigi.LocalTarget(str(self.output_dir() / 'author_sector_classification_stats.csv'))
        }

@requires(AuthorIdentities, AuthorContexts)
class AuthorMetadata(DiversityInScienceTask):
    """
    Compiles metadata about authors, including identities and contexts.
    Bibliographies are used as the source of truth for author names; author context
    is only associated with author names from text. We need to choose how to cross-
    tabulate bib authors with text authors (should use pub keys!)
    """

    def run(self):
        identitiesFile, contextsData = self.input()
        contexts = pd.read_csv(contextsData['df'].path, index_col=0)
        contexts['name'] = contexts['first'].str.lower() + ' ' + contexts['last'].str.lower()
        identities = pd.read_csv(identitiesFile.path, index_col=0).drop_duplicates(['first', 'last'])
        identities['name'] = identities['first'].str.lower() + ' ' + identities['last'].str.lower()
        identities = identities.drop(columns=['first', 'last', 'middle'])
        metadata = contexts.merge(identities, how='left', left_on='name', right_on='name')
        metadata = metadata.drop_duplicates(['pubkey', 'position'])
        metadata.to_csv(self.output().path)

    def output(self):
        return luigi.LocalTarget(str(self.output_dir() / 'authors_with_metadata.csv'))

@requires(PreprocessDocuments, AuthorMetadata)
class PapersWithFirstAuthorMetadata(DiversityInScienceTask):
    """
    Merges the first author's metadata into papers
    """

    def run(self):
        papersFile, authorsFile = self.input()
        papers = pd.read_csv(papersFile['df'].path, index_col=['doc', 'part'])
        authors = pd.read_csv(authorsFile.path, index_col=0)
        firstAuthors = authors[authors.position == 0]
        firstAuthors = firstAuthors.drop(columns=['year', 'venue'])
        papersWithFirstAuthorMetadata = papers.merge(firstAuthors, how='left', left_on='key', right_on='pubkey')
        papersWithFirstAuthorMetadata.to_csv(self.output().path)
        
    def output(self):
        return luigi.LocalTarget(str(self.output_dir() / 'papers_with_first_author_metadata.csv'))
        
@requires(PreprocessDocuments, AuthorMetadata)
class PapersWithAllAuthorMetadata(DiversityInScienceTask):
    """
    Merges the average author's metadata into papers
    """

    def run(self):
        papersFile, authorsFile = self.input()
        papers = pd.read_csv(papersFile['df'].path, index_col=['doc', 'part'])
        authors = pd.read_csv(authorsFile.path, index_col=0)
        authors = authors.drop(columns=['year', 'venue'])
        papersWithAllAuthorMetadata = papers.merge(authors, how='inner', left_on='key', right_on='pubkey')
        papersWithAllAuthorMetadata.to_csv(self.output().path)
        
    def output(self):
        return luigi.LocalTarget(str(self.output_dir() / 'papers_with_all_author_metadata.csv'))

@requires(AuthorMetadata)
class PlotAuthorDistributions(DiversityInScienceTask):
    """
    Creates heatmaps of author identities and contexts
    """
    normalized = luigi.BoolParameter()

    def run(self):
        df = pd.read_csv(self.input().path, index_col=['doc'])
        f, ((ax0, ax1), (ax2, ax3), (ax4, ax5)) = plt.subplots(3,2, figsize=(12,12))
        self.plot_identity_distribution(df, "gender", ax0)
        self.plot_identity_distribution(df, "race_usa", ax1)
        self.plot_identity_distribution(df, "sector", ax2, ['academic', 'corporate', 'military', 'other'])
        self.plot_identity_distribution(df, "continent", ax3, ['Africa', 'America', 'Asia', 'Europe', 'Oceania'])
        self.plot_identity_distribution(df, "race", ax5)
        ax4.axis('off')
        plt.savefig(self.output().path)

    def plot_identity_distribution(self, df, category, ax, groups=None):
        if groups:
            df = df[df[category].isin(groups)]
        cat_dist = df.groupby(["year", category]).size().unstack().T
        if self.normalized:
            cat_dist = cat_dist / df.groupby("year").year.count()
        sns.heatmap(cat_dist.fillna(0), ax=ax, mask=None)
        ax.set_ylabel(category)

    def output(self):
        qualifier = 'normalized' if self.normalized else 'absolute'
        return luigi.LocalTarget(str(self.output_dir() / 'author_distributions_{}.png'.format(qualifier)))

@requires(GeneratePapersDf, EstimateTopics)
class PlotTopTopics(DiversityInScienceTask):
    "Plots the top topics"
    n = luigi.IntParameter(default=10)

    def run(self):
        papersFile, topicsFile = self.input()
        papers = pd.read_csv(papersFile.path, index_col=['doc'])
        topics = pd.read_csv(topicsFile.path, index_col=['doc', 'part'])
        topTopics = (topics.sum(axis=0) / len(topics)).sort_values(ascending=False).reset_index()
        yearlyTopics = topics.join(papers[['year']], on=['doc']).groupby('year').mean()

        f, ax = plt.subplots(figsize=(12, 4))
        ax.set_ylabel("topic distribution")
        tops = topTopics[:self.n]
        yearlyTopics[tops['index']].plot(ax=ax)
        labels = ["{} ({:.2f}% load)".format(t, load * 100) for t, load in zip(tops['index'], tops[0])]
        plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0., labels=labels)
        plt.savefig(self.output().path)

    def output(self):
        return luigi.LocalTarget(str(self.output_dir() / 'top_{}_topics.png'.format(self.n))) 

@requires(PapersWithFirstAuthorMetadata, PapersWithAllAuthorMetadata, EstimateTopics)
class CovariateDivergenceTest(DiversityInScienceTask):
    "Computes Jensen-Shannon divergence test. Do different folks write about different stuff?"

    def run(self):
        firstAuthorPapersFile, allAuthorsPapersFile, topicsFile = self.input()
        papersFirst = pd.read_csv(firstAuthorPapersFile.path, index_col=['doc'])
        papersAll = pd.read_csv(allAuthorsPapersFile.path, index_col=['doc'])
        topics = pd.read_csv(topicsFile.path, index_col=['doc', 'part'])
        conditions = [
            ('gender', 'male', 'female'),
            ('race_usa', 'white', 'asian'),
            ('sector', 'academic', 'corporate'),
            ('continent', 'America', 'Europe'),
            ('continent', 'America', 'Asia'),
        ]
        results = [(cat, p, q, *self.jensen_shannon_distance(papersFirst, topics, cat, p, q, pval=True)) 
                for cat, p, q in conditions]
        results = pd.DataFrame(results, columns=("category", "group1", "group2", "distance", "p"))
        results.to_csv(self.output()['first'].path)

        results = [(cat, p, q, *self.jensen_shannon_distance(papersAll, topics, cat, p, q, pval=True)) 
                for cat, p, q in conditions]
        results = pd.DataFrame(results, columns=("category", "group1", "group2", "distance", "p"))
        results.to_csv(self.output()['all'].path)

    def jensen_shannon_distance(self, papers, topics, category, pVal, qVal, pval=False, sample=100):
        """
        Translates the dataframe and query terms into a form suitable for computing jensen-shannon distance.
        When p is True, also computes the probability of a distance at least this great 
        (null hypothesis=no difference),
        empirically computing the mean and variance of jensen-shannon distances for 
        """
        #categoryTopics = topics.merge(papers[[category]], how='left', left_index=True, 
                #right_index=True).groupby(category)[topics.columns]
        categoryTopics = topics.join(papers[[category]], on=['doc']).groupby(category)[topics.columns]
        p, q = [categoryTopics.get_group(val).sum(axis=0).values for val in [pVal, qVal]]
        js = jensenshannon(p, q)
        if pval: 
            pFrac = (len(papers[papers[category] == pVal])/(len(papers[papers[category] == pVal]) + 
                    len(papers[papers[category] == qVal])))
            samples = [train_test_split(topics, test_size=pFrac) for _ in range(sample)]
            jsSamples = np.array([jensenshannon(_p.sum(axis=0), _q.sum(axis=0)) for _p, _q in samples])
            z = (js - jsSamples.mean()) / jsSamples.std()
            return js, 1- self.z2p(z)

    def z2p(self, z):
        """From z-score return p-value."""
        return 0.5 * (1 + scsp.erf(z / np.sqrt(2)))

    def output(self):
        return {
            'first': luigi.LocalTarget(str(self.output_dir() / 'jensen_shannon_distance_first_author.csv')),
            'all': luigi.LocalTarget(str(self.output_dir() / 'jensen_shannon_distance_all_authors.csv'))
        }
        
@requires(BuildCorpus, BuildModel)
class Visualization(DiversityInScienceTask):
    """
    Prepares a pyLDAvis visualization
    """
    
    def run(self):
        corpusData, modelData = self.input()
        bagsOfWords = MmCorpus(corpusData['corpus'].path)
        dictionary = Dictionary.load(corpusData['dictionary'].path)
        model = LdaMalletPlus.load(modelData['model'].path)
        gensimLdaModel = malletmodel2ldamodel(model, iterations=100)
        ldaVisData = prepare(gensimLdaModel, bagsOfWords, dictionary, mds='tsne')
        pyLDAvis.save_html(ldaVisData, self.output().path)

    def output(self):
        return luigi.LocalTarget(str(self.output_dir() / 'visualization.html'))

class DiachronicJensenShannonDivergence(DiversityInScienceTask):
    """
    Plots Jensen-Shannon divergence between the distributions of two groups over time.
    """

class OddsRatio(DiversityInScienceTask):
    """
    Does something with odds ratios...
    """

@requires(
    CorpusCompletenessReport, 
    CovariateDivergenceTest, 
    PlotModelComparison, 
    Visualization,
    PlotTopTopics, 
    PlotTopicWordLengths
)
class FullAnalysis(DiversityInScienceTask):
    """
    Runs all tasks
    """

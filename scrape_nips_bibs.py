# Many ICML conferences unfortunately don't have standardized ways of getting their bib files.
# These functions get the job done. 

from pybtex.database import BibliographyData, Entry, parse_string, Person, parse_file
import requests
from helpers import *
import json
from proceedings import ProceedingsScrape
import logging
from pathlib import Path
from os.path import dirname, abspath

HERE = Path(dirname(abspath(__file__)))
BIBS_DIR = HERE.parent / "results" / "bibliographies" / "nips"
#NIPS_YEARS = range(1987, 2018)
#NIPS_YEARS = range(2007, 2018)
NIPS_YEARS = [2018]
log = get_logger(__file__, HERE / "nips_scrape.log", logging.INFO)

class NIPSProceedingsReader(ProceedingsScrape):
    baseUrl = "https://papers.nips.cc/"
    urlTemplate = "https://papers.nips.cc/book/advances-in-neural-information-processing-systems{}-{}"
    firstYear = 1987
    def __init__(self, year, log=None):
        self.log = log or logging
        self.year = year
        index = year - self.firstYear
        self.url = self.urlTemplate.format("-{}".format(index) if index else "", year)
        self.log.info("Scraping NIPS {}".format(year))
        self.log.debug("Setting URL to {}".format(self.url))
        self.bib = BibliographyData()        
        self.page = self.get_page()
        self.papers = self.get_papers(self.page)
        for paper in self.papers:
            try:
                entry = self.parse_paper(paper)
                if not entry.key:
                    entry.key = self.generate_key(entry)
                self.bib.entries[entry.key] = entry
                self.log.info("Added {} to NIPS {} bib".format(entry.key, self.year))
            except Exception as e:
                self.log.warning(e)

    def get_papers(self, page):
        paperLinkList = page.find_all("ul")[1]
        paperLinks = paperLinkList.find_all("li")
        paperUrls = [self.baseUrl + l.a.attrs['href'] for l in paperLinks]
        return paperUrls

    def parse_paper(self, paperUrl):
        self.log.debug("Getting url for paper: {}".format(paperUrl))
        paper = get_and_parse(paperUrl)
        abstract = paper.find(class_="abstract").text.replace('{', '').replace('}', '')
        if abstract == "Abstract Missing":
            self.log.debug("  - No abstract")
            abstract = None
        bibtexUrl = paper.find('a', text="[BibTeX]").attrs['href']
        response = requests.get(self.baseUrl + bibtexUrl)
        if response.ok:
            newBib = parse_string(response.text, 'bibtex')
            ((key, entry),) = newBib.entries.items()
            entry.key = key
            entry.fields['pdf'] = entry.fields['url']
            if abstract:
                entry.fields['abstract'] = abstract
            return entry
        else:
            raise IOError("Error fetching url: {}".format(bibtexUrl))

if __name__ == '__main__':
    for year in NIPS_YEARS:
        reader = NIPSProceedingsReader(year, log=log)
        reader.bib.to_file(str(BIBS_DIR / "nips_{}.bib".format(year)))

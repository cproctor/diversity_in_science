from pybtex.database import BibliographyData, Entry, parse_string, Person, parse_file
from bs4 import BeautifulSoup
from citation_parser import ACMProceedingsParser
import requests
import re
from helpers import *
import logging

# TODO could subclass this from proceedings.ProceedingsReader
class ACMProceedings:
    "Processes an ACM Digital Library page for conference proceedings into a bibliography file"

    entry_type = "InProceedings"

    def __init__(self, url, baseCitationFields=None, parser=None, log=None):
        self.url = url
        self.baseCitationFields = baseCitationFields or {}
        self.parser = parser or ACMProceedingsParser()
        self.log = log or logging
        self.build_bibliography()

    def get_page(self):
        return get_and_parse(self.url)

    def build_bibliography(self):
        self.bib = BibliographyData()

        page = self.get_page()
        proceedings = page.find_all('table', class_="text12")[1]
        rows = proceedings.find_all('tr')
        for title, authors, pages, doi, pdfUrl, abstract in batched(rows, 6):
            fields = {
                "title": self.parser.parse_title(title.text),
                "pages": self.parser.parse_pages(pages.text),
                "doi": self.parser.parse_doi(doi.text),
                "abstract": self.parser.parse_abstract(abstract.text)
            }
            fields.update(self.baseCitationFields)
            persons = self.parser.parse_people(authors.text)
            entry = Entry(self.entry_type, fields=fields, persons=persons)
            key = self.generate_key(entry)
            self.bib.entries[key] = entry

    def generate_key(self, entry, duplicate=0):
        "Generates a bibliography entry key"
        authors = entry.persons.get('author', [])
        if any(authors):
            if authors[0].last_names:
                auth = " ".join(authors[0].last_names)
            elif authors[0].first_names:
                auth = "Anon"
        else:
            auth = "Anon"
        year = entry.fields.get('year', 'Undated')
        key = auth + year
        if duplicate:
            key += character(duplicate)
        if self.bib.entries.get(key):
            return self.generate_key(entry, duplicate+1)
        else:
            return key

# MessyResearch::Segmentation
# (c) 2019 Chris Proctor
# Tools for segmenting PDF text sections

import re

def spacey_pattern(word):
    return '[^\\w]' + ''.join(w + '\\s*' for w in word) + '$'

PREAMBLE_SENTINEL_PATTERN = spacey_pattern('abstract') + '|' + spacey_pattern('introduction')
REFERENCES_SENTINEL_PATTERN = spacey_pattern('references')

def split_preamble(text, stats=False):
    "Splits text string into the preamble and the rest, if possible. If stats, reports split position instead"
    return _split(text, PREAMBLE_SENTINEL_PATTERN, stats=stats) or ("", text)
    
def split_references(text, stats=False):
    "Splits text string into the preamble and the rest, if possible. If stats, reports split position instead"
    return _split(text, REFERENCES_SENTINEL_PATTERN, stats=stats) or (text, "")

def _split(text, pattern, stats=False):
    "A helper which splits on a pattern, optionally returning stats instead of the split strings"
    match = re.search(pattern, text, re.M | re.I)
    if stats:
        return {
            'position': match.start() if match else None,
            'length': len(text)
        }
    else:
        return (text[:match.start()], text[match.start():]) if match else None

def strip_preamble_and_references(text):
    preamble, rest = split_preamble(text)
    body, references = split_references(rest)
    return body

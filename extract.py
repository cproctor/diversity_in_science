# A script to extract text from PDFs. Run python extract.py --help 
# to see help and options

from pathlib import Path
from os.path import dirname, abspath
from argparse import ArgumentParser
from bibtools.reader import bib_dir_reader
from helpers import get_logger, get_path
import logging
import requests
import subprocess

HERE = Path(dirname(abspath(__file__)))
BIBS_DIR = HERE.parent / "results" / "bibliographies" / "icml"
PDFS_DIR = HERE.parent / "results" / "pdfs" / "icml"
TEXT_DIR = HERE.parent / "results" / "text" / "icml"
LOG = HERE.parent / "logs" / "extract.log"
PDFMINER_SCRIPT = ["python", "pdf2txt.py"]

def download_pdf(entry, pdfs_dir, log):
    try:
        response = requests.get(entry.fields['pdf'])
        if response.ok:
            with open(get_path(entry.key, pdfs_dir, 'pdf'), 'wb') as f:
                f.write(response.content)
                log.info("[{}] Downloaded pdf".format(entry.key, entry.fields.get('title', "NO TITLE")))
        else:
            log.warning("[{}] Error downloading pdf".format(entry.key))
            log.debug(response)
    except Exception as e:
        log.error("[{}] Exception raised while downloading pdf: {}".format(e, entry.fields['pdf']))

def extract_text_from_pdf(entry, pdfs_dir, text_dir, log):
    "Uses pdfminer to extract text from pdf"
    #try:
    if True:
        with open(get_path(entry.key, text_dir, 'txt'), 'w') as out:
            # INstead, capture output and check that it's not empty.
            subprocess.run(PDFMINER_SCRIPT + [str(get_path(entry.key, pdfs_dir, 'pdf'))],
                    stdout=out, encoding='utf-8')
        log.info("[{}] Extracted text from pdf".format(entry.key))
    #except Exception as e:
        #log.error("[{}] Error extracting text from pdf".format(entry.key))
        #log.debug(e)

if __name__ == '__main__':
    logLevels = {
        "debug": logging.DEBUG,
        "info": logging.INFO,
        "warning": logging.WARNING,
        "error": logging.ERROR
    }
    parser = ArgumentParser("Extracts text from pdfs")
    parser.add_argument('-f', '--force', action='store_true', 
            help="Force extraction of text, even when it already exists")
    parser.add_argument('-d', '--skip_download', action='store_true', 
            help="Skip downloads; only extract text")
    parser.add_argument('-s', '--skip_extract', action='store_true', 
            help="Downolad pdfs but skip extraction")
    parser.add_argument('-b', '--bibs_dir', default=str(BIBS_DIR),
            help="Directory of .bib files to process")
    parser.add_argument('-p', '--pdfs_dir', default=str(PDFS_DIR),
            help="Directory for .pdf files")
    parser.add_argument('-t', '--text_dir', default=str(TEXT_DIR),
            help="Directory for output .txt files")
    parser.add_argument('-y', '--year', help="Year to download and process", type=int)
    parser.add_argument('-l', '--log_file', default=str(LOG), 
            help="File to log to")
    parser.add_argument('-e', '--log_level', choices=logLevels.keys(), default='info')
    args = parser.parse_args()

    log = get_logger(__file__, args.log_file, logLevels[args.log_level])
    log.info("Starting run")
    for entry in bib_dir_reader(args.bibs_dir):
        if args.year and args.year != int(entry.fields.get('year', 0)): 
            log.debug("Skipping {} because its year is not {}".format(entry.key, args.year))
            continue
        if not args.skip_download:
            if not entry.fields.get('pdf'):
                log.warning("[{}] Can't download; no pdf field".format(entry.key))
                continue
            if args.force or not get_path(entry.key, args.pdfs_dir, 'pdf').exists():
                download_pdf(entry, args.pdfs_dir, log)
            else:
                log.debug("[{}] Skipping download; file already exists".format(entry.key))
        if not args.skip_extract:
            pdfpath = get_path(entry.key, args.pdfs_dir, 'pdf')
            if not pdfpath.exists():
                log.warning("[{}] Can't extract; no pdf file".format(entry.key))
                continue
            if args.force or not get_path(entry.key, args.text_dir, 'txt').exists():
                extract_text_from_pdf(entry, args.pdfs_dir, args.text_dir, log)
            else:
                log.debug("[{}] Skipping extraction; file already exists".format(entry.key))

import requests
import requests_cache

requests_cache.install_cache()

# ACL (not the broader ACL Web) offers well-structured bib files for its conferences, 
# so all we need to do is download them.

for year in range(2017, 2001, -1):
    url = "http://www.aclweb.org/anthology/P/P{0}/P{0}-1.bib".format(str(year)[-2:])
    saveAs = "acl/acl_{}.bib".format(year)
    resp = requests.get(url)
    if resp.ok:
        with open(saveAs, 'w') as outf:
            outf.write(resp.text)
    else:
        print("Error fetching {}".format(url))

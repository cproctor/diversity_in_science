from pybtex.database import BibliographyData, Entry, parse_string, Person, parse_file
from bs4.element import NavigableString
from helpers import *
import logging
import requests_cache


class ProceedingsReader:
    "A base class capable of building a proceedings bibliography from sources"
    basicInfo = None
    entryType = "InProceedings"

    def __init__(self, log=None):
        self.log = log or logging

    def generate_key(self, entry, duplicate=0):
        "Generates a bibliography entry key"
        authors = entry.persons.get('author', [])
        if any(authors):
            if authors[0].last_names:
                auth = " ".join(authors[0].last_names)
            elif authors[0].first_names:
                auth = "Anon"
            else:
                auth = "Anon"
        else:
            auth = "Anon"
        year = entry.fields.get('year', 'Undated')
        key = auth + year
        key = key.replace(' ', '')
        if duplicate:
            key += character(duplicate)
        if self.bib.entries.get(key):
            return self.generate_key(entry, duplicate+1)
        else:
            return key

    def expect(self, prop, val):
        if val and not (isinstance(val, str) and not val.strip()):
            self.log.debug("{}: {}".format(prop, val.strip() if isinstance(val, str) else val))
        else:
            self.log.warn("No {} found".format(prop))

class ProceedingsScrape(ProceedingsReader):
    "Builds proceedings from a webpage."
    url = "url_or_filename.html"

    def __init__(self, log=None):
        self.log = log or logging
        self.bib = BibliographyData()        
        self.page = self.get_page()
        self.papers = self.get_papers(self.page)
        for paper in self.papers:
            entry = self.parse_paper(paper)
            if entry.key:
                self.bib.entries[entry.key] = entry
            else:
                key = self.generate_key(entry)
                self.bib.entries[key] = entry

    def get_page(self):
        return get_and_parse(self.url)

    def get_papers(self, page):
        return []
        
    def parse_paper(self, paper):
        fields = dict(self.basicInfo)
        self.add_field(fields, "title", self.get_paper_title(paper))
        self.add_field(fields, "abstract", self.get_paper_abstract(paper))
        self.add_field(fields, "url", self.get_paper_url(paper))
        self.add_field(fields, "pdf", self.get_paper_url(paper))
        persons = {}
        self.add_field(persons, "author", self.get_paper_authors(paper))
        return Entry(self.entryType, fields=fields, persons=persons)

    def add_field(self, fields, fieldName, fieldValue):
        self.expect(fieldName, fieldValue)
        if fieldValue: fields[fieldName] = fieldValue

    def get_paper_title(self, paper):
        raise NotImplementedError()
    def get_paper_authors(self, paper):
        raise NotImplementedError()
    def get_paper_abstract(self, paper):
        raise NotImplementedError()
    def get_paper_url(self, paper):
        raise NotImplementedError()

class ICML2004Proceedings(ProceedingsScrape):
    url = "https://icml.cc/Conferences/2004/proceedings.html"
    papersBaseUrl = "https://icml.cc/Conferences/2004/"
    basicInfo = {
        "booktitle": "Proceedings of the 21st international machine learning conference",
        "series": "ICML '04",
        "year": "2004",
        "isbn": "978-1-57735-189-4",
        "location": "Banff, Alberta, CA",
        "publisher": "ACM",
        "address": "New York, NY, USA"
    }
    def get_papers(self, page):
        rows = page.find(class_="proceedings").find_all('tr')
        title, links, authors = zip(*batched(rows, 3))
        details = [get_and_parse(self.papersBaseUrl + l.a.attrs['href']) for l in links]
        return zip(title, links, authors, details)
        
    def get_paper_title(self, paper):
        title, links, authors, details = paper
        return title.text.strip()

    def get_paper_authors(self, paper):
        title, links, authors, details = paper
        authors = filter(lambda e: isinstance(e, NavigableString), authors.find_all('td')[1].children)
        return [Person(a.strip("\n -")) for a in authors]

    def get_paper_abstract(self, paper):
        title, links, authors, details = paper
        return details.find_all('tr')[2].text.replace("\r", " ")

    def get_paper_url(self, paper):
        title, links, authors, details = paper
        return self.papersBaseUrl + links.find_all('a')[1].attrs['href']
        
class ICML2003Proceedings(ProceedingsScrape):
    url = "https://aaai.org/Library/ICML/icml03contents.php"
    basicInfo = {
        "booktitle": "Proceedings of the twentieth international conference on machine learning",
        "series": "ICML '03",
        "year": "2003",
        "isbn": "978-1-57735-189-4",
        "location": "Washington, DC",
        "publisher": "AAAI Press",
        "address": "Menlo Park, CA"
    }

    def get_papers(self, page):
        papersBaseUrl = "https://aaai.org/Library/ICML/"
        papers = page.find(class_='content').find_all('p', class_='left')[3:-1]
        detailUrls = [papersBaseUrl + p.a.attrs['href'] for p in papers]
        paperDetails = [get_and_parse(du) for du in detailUrls]

        # Each 'paper' is actually a tuple: the paper's tag from the index page and 
        # The html from its detail page. Thus, "p, d = page" in the methods below.
        return zip(papers, paperDetails) 

    def get_paper_title(self, paper):
        p, d = paper
        return p.a.text
    def get_paper_authors(self, paper):
        p, d = paper
        authors = p.a.find_next('i').text
        return [Person(a.strip()) for a in authors.split(",")] 
    def get_paper_abstract(self, paper):
        p, d = paper
        abstract = d.find_all('p')[1].text
        return remove_junk(abstract)
    def get_paper_url(self, paper):
        p, d = paper
        return url_backup("https://aaai.org/Library/ICML/2003/", d.h1.a.attrs['href'])

class ICML2002Proceedings(ProceedingsScrape):
    url = "temp/icml2002.html"
    basicInfo = {
        "booktitle": "Proceedings of the nineteenth international conference on machine learning",
        "series": "ICML '02",
        "year": "2002",
        "isbn": "1-55860-873-7",
        "publisher": "Morgan Kaufmann Publishers",
        "address": "San Francisco, CA, USA"
    }   
    def get_papers(self, page):
        return batched(page.find_all('tr'), 4)
    def parse_paper(self, paper):
        fields = dict(self.basicInfo)
        self.add_field(fields, "title", self.get_paper_title(paper))
        self.add_field(fields, "pages", self.get_paper_pages(paper))
        persons = {}
        self.add_field(persons, "author", self.get_paper_authors(paper))
        return Entry(self.entryType, fields=fields, persons=persons)
    def get_paper_title(self, paper):
        title, authors, pages, _ = paper
        return title.text.strip()
    def get_paper_authors(self, paper):
        title, authors, pages, _ = paper
        return [Person(a.text.strip()) for a in authors.find_all('a')]
    def get_paper_pages(self, paper):
        title, authors, pages, _ = paper
        match = re.search("(\d+)\s*-\s*(\d+)", pages.text)
        return "{}-{}".format(match.group(1), match.group(2))

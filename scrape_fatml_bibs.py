from proceedings import ProceedingsReader
from bs4.element import NavigableString
from helpers import *
import requests_cache
from urlpath import URL
from urllib import parse
import inflect

# Scrapes all the papers from FatML
p = inflect.engine()

class FatMLConferenceScrape(ProceedingsReader):

    def __init__(self, year, url, log=None):
        self.url = URL(url)
        self.year = year
        self.basicInfo = {
            "booktitle": "Proceedings of the {} Workshop on Fairness, Accountability, and Transparency in Machine Learning.".format(p.ordinal(self.year - 2013)),
            "year": str(self.year)
        }
        self.log = log or logging
        self.bib = BibliographyData()        
        self.page = get_and_parse(self.url)
        for paperEl in self.page.find_all('p'):
            authors = self.local_text(paperEl).replace(':', '').replace(' and ', ', ')
            if paperEl.find('a'):
                title = paperEl.find('a').text
                pdfUrl = URL(parse.unquote(paperEl.find('a').attrs['href']).replace('\\_', '_')
                if not pdfUrl.is_absolute():
                    pdfUrl = pdfUrl.with_hostinfo(self.url.hostname)
            else: 
                print("Nothing here...")
                print(paperEl)
                continue
            fields = dict(self.basicInfo)
            fields['title'] = title
            fields['pdf'] = str(pdfUrl)
            fields['url'] = str(pdfUrl)
            persons = {'author': [Person(a) for a in authors.split(',')]}
            entry = Entry(self.entryType, fields=fields, persons=persons)
            entry.key = self.generate_key(entry)
            self.bib.entries[entry.key] = entry

    def local_text(self, el):
        return ''.join(e for e in el if isinstance(e, NavigableString))

confs = [
    (2015, "http://www.fatml.org/schedule/2015/page/papers-2015"),
    (2016, "http://www.fatml.org/schedule/2016/page/papers"),
    (2017, "http://www.fatml.org/schedule/2017/page/papers-2017"),
    (2018, "http://www.fatml.org/schedule/2018/page/papers-2018")
]

if __name__ == '__main__':
    from pathlib import Path
    from os.path import dirname, abspath
    HERE = Path(dirname(abspath(__file__)))
    BIBS_DIR = HERE.parent / "results" / "bibliographies" / "fatml"
    scrapes = {}
    for year, url in confs:
        scrape = FatMLConferenceScrape(year, url)
        scrapes[year] = scrape
        scrape.bib.to_file(BIBS_DIR / "fatml_{}.bib".format(year))
        print(scrape.bib.to_string('bibtex'))

import pybtex
from pybtex.database import BibliographyData, Entry, parse_string, Person, parse_file
from pybtex.scanner import TokenRequired
from bs4 import BeautifulSoup
import requests
from os.path import dirname, abspath
from pathlib import Path
import re
from helpers import *
from tqdm import tqdm
from fuzzywuzzy import process
from acm_proceedings import ACMProceedings
import json
from proceedings import ProceedingsScrape
from urllib.parse import urljoin

HERE = Path(dirname(abspath(__file__)))
BIBS_DIR = HERE.parent / "results" / "bibliographies" / "aaai"

class InvalidPaperError(Exception):
    pass

class AAAIReader:
    """
    Implements a three-layer scraper, 
    """
    indexUrl = "https://aaai.org/Library/AAAI/aaai-library.php"
    
    def __init__(self, log, years=None):
        self.page = get_and_parse(self.indexUrl)
        self.log = log
        confs = self.get_conferences()
        if years:
            if not isinstance(years, list): 
                years = [years]
            confs = [(year, url) for year, url in confs if year in years]
        self.conferenceReaders = [AAAIConferenceReader(year, url, log) for year, url in confs]

    def get_conferences(self):
        return [(int(a.text), urljoin(self.indexUrl, a.attrs['href'])) 
                for a in self.page.find(class_='nine').find_all('a')]

    def scrape_bibs(self):
        for r in self.conferenceReaders:
            r.scrape_bib()
        
class AAAIConferenceReader(ProceedingsScrape):
    def __init__(self, year, url, log):
        self.log = log
        self.bib = BibliographyData()        
        self.year = year
        self.log.info("Initialized {}".format(self))
        self.url = url
        self.page = self.safe_parse(self.url)
        self.confInfo = self.get_conf_info()
        self.papers = self.get_papers(self.page)
        for i, paper in enumerate(self.papers):
            self.log.info("Generating bibliography entry for paper {}/{}".format(i, len(self.papers)))
            try:
                entry = self.parse_paper(paper, year=year)
            except InvalidPaperError:
                self.log.error("Could not parse paper {}".format(paper))
                continue
            key = self.generate_key(entry)
            self.bib.entries[key] = entry
        self.bib.to_file(BIBS_DIR / "aaai_{}.bib".format(self.year))

    def get_papers(self, page):
        paperLinks = [urljoin(self.url, a.attrs.get('href')) for a in page.find(class_='content').find_all('a')]
        trashLinks = {
            2017: 1,
            2008: 8,
            2007: 12,
            2002: 7
        }
        return [p for p in paperLinks[trashLinks.get(self.year):] if p and not p.endswith('pdf')]

    def parse_paper(self, paperUrl, year=None):
        if year <= 2008:
            return self.parse_paper_pre_2009(paperUrl)
        try:
            self.log.debug("Parsing paper at {}".format(paperUrl))
            paperID = paperUrl.split('/')[-1]
            entry = self.get_bibtex_entry(paperID)

            framelessUrl = "https://aaai.org/ocs/index.php/AAAI/AAAI{}/paper/viewPaper/{}"
            self.log.debug("Using frameless URL: {}".format(framelessUrl.format(str(self.year)[2:], paperID)))
            paper = self.safe_parse(framelessUrl.format(str(self.year)[2:], paperID))
            pdfLink = paper.find('a', text='PDF')
            if pdfLink:
                pdfUrl = pdfLink['href']
                pdfUrl = pdfUrl.replace("view", "download")
                entry.fields['pdf'] = pdfUrl
            return entry
        except (AttributeError, TokenRequired):
            raise InvalidPaperError()

    def parse_paper_pre_2009(self, paperUrl):
        "TLC for 2008 and older"
        def debrace(s):
            return s.replace('{', '').replace('}', '')
        try:
            self.log.debug("Parsing paper at {}".format(paperUrl))
            paperSlug = paperUrl.split('/')[-1]
            paperIDMatch = re.match("aaai\d{2}-(\d+).php", paperSlug)
            if paperIDMatch:
                paperID = paperIDMatch.groups(1)
                self.log.debug("Paper ID is {}".format(paperID))
            else:
                self.log.error("Could not parse paper ID")
            paper = self.safe_parse(paperUrl)
            fields = self.get_conf_info()
            pees = paper.find_all('p')
            self.add_field(fields, "title", debrace(paper.find('h1').text))
            self.add_field(fields, 'url', paperUrl)
            self.add_field(fields, 'pdf', self.get_paper_pdf(paper))
            self.add_field(fields, "abstract", debrace(pees[1].text))
            authorsString = debrace(pees[0].text)
            authors = [Person(a.strip()) for a in authorsString.split(",")]
            return Entry(self.entryType, fields=fields, persons = {'author': authors})
        except IOError:
            self.log.error("Could not parse paper at {}".format(paperUrl))
            raise InvalidPaperError()

    def get_paper_pdf(self, paper):
        a = paper.find('a')
        if a:
            return urljoin(self.url, a['href'])
        else:
            return ""

    def get_bibtex_entry(self, paperID):
        url = "https://aaai.org/ocs/index.php/AAAI/AAAI{}/rt/captureCite/{}/0/BibtexCitationPlugin"
        citePage = self.safe_parse(url.format(str(self.year)[2:], paperID))
        bib = self.safe_parse_bibtex(citePage.find('pre').text)
        entry = list(bib.entries.values())[0]
        return entry
        
    def get_conf_info(self):
        return {
            "booktitle": "Proceedings of AAAI {}".format(self.year),
            "series": "AAAI '{}".format(str(self.year)[2:]),
            "year": str(self.year),
        }

    def identify_paper_page_version(self, page):
        if page.find(id='breadcrumb'):
            return 1

    def parse_paper_v1(self, paper, paperUrl):
        fields = dict(self.get_conf_info())
        self.add_field(fields, "title", self.get_paper_title_v1(paper))
        self.add_field(fields, "url", paperUrl)
        self.add_field(fields, "pdf", self.get_paper_pdf_v1(paper))
        persons = {}
        self.add_field(persons, "author", self.get_paper_authors_v1(paper))
        return Entry(self.entryType, fields=fields, persons=persons)
    
    def get_paper_title_v1(self, paper):
        elem = paper.find(id='title').text
        return elem.text if elem else None

    def get_paper_pdf_v1(self, paper):
        elem = paper.find('a', string='PDF')
        return elem.attrs['href'] if elem else None

    def get_paper_author_v1(self, paper):
        elem = paper.find(id='author')
        return elem.text if elem else None

    def safe_parse_bibtex(self, bibtex_string):
        try:
            return parse_string(bibtex_string, 'bibtex')
        except pybtex.scanner.PrematureEOF:
            self.log.warning("Bibtex string crashed, trying to recover:\n{}".format(bibtex_string))
            unbalancedBracesPattern = "=\s*\{(.*)(\}|\{)(.*)\}"
            replPattern = r"= {\1\3}"
            bibtex_fixed = re.sub(unbalancedBracesPattern, replPattern, bibtex_string)
            return parse_string(bibtex_fixed, 'bibtex')

    def safe_parse(self, url):
        try: 
            return get_and_parse(url)
        except (requests.exceptions.MissingSchema, FileNotFoundError):
            url = self.fix_paper_url(url)
            return get_and_parse(url)

    def fix_paper_url(self, url):
        if url.startswith("https:/aaai.org"):
            newUrl =  "https://aaai.org" + url[15:]
            self.log.debug("Using fixed url: {}".format(newUrl))
            return newUrl
        else:
            self.log.warning("Could not fix url: {}".format(url))
            return url

    def __str__(self):
        return "<AAAI {} Conference reader>".format(self.year)

if __name__ == '__main__':
    log = get_logger(__file__, 'bib_updates.log', logging.DEBUG)
    #root = AAAIReader(log, years=[2007])
    root = AAAIReader(log, years=[1998, 1997, 1996, 1990, 1994, 1993, 1992, 1991, 1988, 1987, 1986, 1984, 1983, 1982, 1980])

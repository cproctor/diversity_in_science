
from pybtex.database import BibliographyData, Entry, parse_string, Person
from bs4 import BeautifulSoup
import requests
import re
import requests_cache
import logging
from pathlib import Path

# Careful! This changes the state of the requests library to cache all requests.
# This will help reduce load on scraped servers, and allow us to write simpler scrapes,
# but it could be the source of subtle bugs.
requests_cache.install_cache()

def batched(iterable, n):
    """
    iterates over iterable in batches of n. For example: 

        >>> batched(range(9), 3)  
        [[0,1,2],[3,4,5],[6,7,8]]

    """
    it = iter(iterable)
    try:
        while True:
            yield [next(it) for _ in range(n)]
    except StopIteration:
        pass

def entry_to_dict(e):
    "Converts a pybtex Entry to a dict which includes its key"
    d = e.fields
    d['key'] = e.key
    return d

def first_line(s):
    "Returns the first line of the string"
    return s.split("\n")[0]

def character(i, start='a'):
    "Zero-indexed ordinal characters"
    return chr(i+ord(start))

def generate_key(entry, bibliography=None, duplicate=0):
    "Generates a bibliography entry key"
    authors = entry.persons.get('author', [])
    if any(authors):
        if authors[0].first_names:
            auth = " ".join(authors[0].first_names)
        elif authors[0].last_names:
            auth = "Anon"
    else:
        auth = "Anon"
    year = entry.fields.get('year', 'Undated')
    key = auth + year
    if duplicate:
        key += character(duplicate)

    if bibliography and key in bibliography.entries.keys():
        return generate_key(entry, bibliography, duplicate+1)
    else:
        return key

def get_and_parse(url):
    try:
        response = requests.get(url)
        if response.ok:
            text = response.text
        else:
            raise IOError("Could not fetch {}".format(url))
    except requests.exceptions.MissingSchema:
        with open(url) as infile:
            text = infile.read()
    return BeautifulSoup(text, 'html.parser')

def parse_authors(paper):
    authStrings = first_line(paper.find(class_="authors").text)
    return [Person(a.strip()) for a in authStrings.split(",")]

def parse_abstract(paper):
    return first_line(paper.find(class_="abstract").text).replace("Abstract: ", "")

def parse_remote_bib(url, format='bibtex'):
    response = requests.get(url)
    if not response.ok:
        raise IOError("Could not fetch {}".format(url))
    return parse_string(response.text, format)

def url_backup(baseUrl, relativeUrl):
    "Resolves .. in relative urls"
    truthy = lambda x: x
    base = list(filter(truthy, baseUrl.split("/")))
    rel = list(filter(truthy, relativeUrl.split("/")))
    while any(rel) and rel[0] == "..":
        rel = rel[1:]
        base = base[:-1]
    return "/".join(base + rel)

def remove_junk(string):
    return string.replace("{", "").replace("}", "")

def get_logger(logName, fileName, level=logging.DEBUG):
    "Gets a preconfigured logger"
    log = logging.getLogger(logName)
    log.setLevel(level)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    fh = logging.FileHandler(fileName)
    sh = logging.StreamHandler()
    for h in [fh, sh]:
        h.setLevel(level)
        h.setFormatter(formatter)
        log.addHandler(h)
    return log
    
def get_path(bibtex_key, dr, ext):
    return Path(dr) / "{}.{}".format(bibtex_key, ext)








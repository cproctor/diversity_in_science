# Tools for working with messy text of academic papers (as pulled from a PDF)

import os
from pathlib import Path

localAan = "/Users/chris/Documents/4-PhD/Research/Collaborations/mcfarland/RA/aan/papers_text"

def aclweb_text_reader(n=None, baseDir=localAan):
    "Yields (messy) full text of ACLweb articles"
    for i, fn in enumerate(os.listdir(baseDir)):
        if i == n: break
        yield open(Path(baseDir) / fn)

class Paper:
    def __init__(self, text):
        "Text should be an iterable of strings representing text from an academic paper"
        self.text = text
        self.title = self.extract_title()
        self.headings = self.extract_headings()

    def extract_title(self):
        return 

if __name__ == "__main__":
    for text in aclweb_text_reader(10):
        print(next(text).strip())
        

import pandas as pd
from itertools import islice
import sys
sys.path.append('..')
from extract_authors_from_bibs import name_cleaner_factory

clean = name_cleaner_factory()

def entries_to_csv(entries, filename):
    "Saves an iterable of pybtex.database.Entry to a csv"
    entries_to_df(entries).to_csv(filename)

def entries_to_df(entries, authors=1):
    "Converts an iterable of pybtex.database.Entry to a pandas.DataFrame"
    df = pd.DataFrame.from_dict(entry_to_dict(e, authors) for e in entries)
    df = clean_df(df)
    return df

def clean_df(df):
    "Cleans up some common cruft"
    if "URL" in df.columns: # Merging column URL into url
        df = df.assign(url=df.apply(lambda row: row['url'] or row['URL'], axis=1))
        df = df.drop(columns=['URL'])
    return df

def persons_to_dict(persons, n=1):
    "Formats the first n of pybtex.database.Person as a dict"
    pd = {}
    for i in range(n):
        try:
            pd["author{}_first".format(i)] = clean(' '.join(persons[i].first_names))
            pd["author{}_last".format(i)] = clean(' '.join(persons[i].last_names))
        except IndexError:
            pd["author{}_first".format(i)] = None
            pd["author{}_last".format(i)] = None
    return pd

def entry_to_dict(entry, authors=1):
    "Converts a pybtex.database.Entry to a {str:str} dict"
    d = entry.fields
    d['key'] = entry.key
    d.update(persons_to_dict(entry.persons.get('author', []), n=authors))
    return {str(k) : str(v) for k, v in d.items()}

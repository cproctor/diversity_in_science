from pathlib import Path
from os.path import dirname, abspath
from bibtools.reader import bib_dir_reader
from pybtex.database import BibliographyData
from itertools import chain
import csv
from tqdm import tqdm
from pylatexenc.latex2text import latex2text
from helpers import get_logger
from pylatexenc.latexwalker import LatexWalkerParseError
from unidecode import unidecode
from argparse import ArgumentParser
import re
import logging
import pandas as pd

def author_reader(bib_reader, position=False):
    "Yields (author, entry) tuples from bibliographies"
    for entry in bib_reader:
        if position:
            l = len(entry.persons.get('author', [])) 
            for i, author in enumerate(entry.persons.get('author', [])):
                yield i, l, author, entry
        else:
            for author in entry.persons.get('author', []):
                yield author, entry

def author_dict(author, i, l, entry, nameCleaner):
    "Flattens and cleans a pybtex.Person into a dict"
    return {
        'first': nameCleaner(' '.join(author.first_names)),
        'middle': nameCleaner(' '.join(author.middle_names)),
        'last': nameCleaner(' '.join(author.last_names)),
        'pubkey': entry.key,
        'year': entry.fields.get('year'),
        'venue': entry.fields.get('booktitle'),
        'position': i,
        'num_authors': l,
    }

def repair_latex(string):
    "Fix malformed latex"
    repaired = re.sub(r"\\$", "", string)
    repaired = re.sub(r"\\{(.)}", r"\1", repaired)
    return repaired

outputChoices = ['ascii', 'utf8', 'latex']

def name_cleaner_factory(log=None, output='ascii', catch_errors=True):
    "Configures a cleaning function for names"
    log = log or logging
    if output not in outputChoices:
        raise ValueError("output provided ({}) must be one of {}".format(output, outputChoices))
    def clean(name):
        try:
            log.debug("Cleaning {}".format(name.__repr__()))
            latex = repair_latex(name)
            if output == 'latex': return latex
            utf8 = latex2text(latex)
            if output == 'utf8': return utf8
            asci = unidecode(utf8)
            return asci
        except Exception as e:
            if catch_errors:
                log.warn("Error cleaning {}: {}".format(name.__repr__(), e))
            else:
                raise e
    return clean

def extract_authors_from_bibs(bib_dirs, fmt, outfile, log=None, unique=False):
    log = log or logging
    log.info("Extracting authors from bibliographies")
    bibs = chain(*map(bib_dir_reader, bib_dirs))
    clean = name_cleaner_factory(log=log, output=fmt, catch_errors=True)
    authors = pd.DataFrame(author_dict(a, i, l, e, clean) for i, l, a, e in author_reader(bibs, position=True))
    if unique:
        uniqueTogether = ['first', 'last', 'middle']
        authors = authors[uniqueTogether].groupby(uniqueTogether).first().reset_index()
    authors.to_csv(outfile)

if __name__ == '__main__':
    logLevels = {
        "debug": logging.DEBUG,
        "info": logging.INFO,
        "warning": logging.WARNING,
        "error": logging.ERROR
    }
    HERE = Path(dirname(abspath(__file__)))
    ICML_BIBS_DIR = HERE.parent / "results" / "bibliographies" / "icml"
    NIPS_BIBS_DIR = HERE.parent / "results" / "bibliographies" / "nips"
    OUTPUT_FILE = HERE.parent / "results" / "authors"/"authors_from_bibs.csv"
    LOG_FILE = HERE.parent / "logs" / "extract_authors_from_bibs.log"
    parser = ArgumentParser("Extract authors from bibtex bibliographies into a CSV")
    parser.add_argument("-b", "--bib_dirs", nargs='*', default=[ICML_BIBS_DIR, NIPS_BIBS_DIR],
            help="Directories containing bibliographies to parse")
    parser.add_argument('-f', '--format', choices=outputChoices, default='ascii',
            help="Format of output text")
    parser.add_argument('-o', '--outfile', default=OUTPUT_FILE, 
            help="File to write csv to")
    parser.add_argument('-u', '--unique', action='store_true', 
            help="Emit one row per author")
    parser.add_argument('-l', '--log_file', default=str(LOG_FILE), 
            help="File to log to")
    parser.add_argument('-e', '--log_level', choices=logLevels.keys(), default='debug')
    args = parser.parse_args()

    log = get_logger(__file__, args.log_file, logLevels[args.log_level])
    extract_authors_from_bibs(args.bib_dirs, args.format, args.outfile, log, args.unique)
